var winston = require('winston');
var moment = require('moment');

// general logger
var generalLogger = new (winston.Logger)({
    transports: [
      new (winston.transports.Console)(
      {
        timestamp: function() {
            return moment().format('DD.MM.YYYY HH:mm:ss.SSSS');
        },
        colorize: true,
        level: "debug",
        formatter: function(options){
            return "[" + options.timestamp() + "] " + options.level.toUpperCase() + " - " + options.message;
        }
      })
    ]
});

// logger dedicated to performance evaluation messages
var performanceLogger = new (winston.Logger)({
    transports: [
      new (winston.transports.Console)(
      {
        timestamp: function() {
            return moment().format('DD.MM.YYYY HH:mm:ss.SSSS');
        },
        colorize: true,
        level: "info",
        formatter: function(options){
            return "[" + options.timestamp() + "] [E2EE] " + options.message;
        }
      })
    ]
});

module.exports.logger=generalLogger;
module.exports.perfLogger=performanceLogger;