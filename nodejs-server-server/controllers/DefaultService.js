'use strict';

var crypto = require('../e2eecrypto');
var e2ee = require('../e2ee');

exports.decryptPOST = function(args, host, res, next) {
  var objectName = args.payload.value.objectName
  var destContainerName = args.payload.value.location.containerName
  var destObjectName = args.payload.value.location.objectName
  
  var callbackUrl = args.payload.value.callbackURL
  e2ee.crypto.downloadFile(objectName, destContainerName, destObjectName, host, callbackUrl);
  res.end();
}

exports.encryptPOST = function(args, host, res, next) {
  var containerName = args.payload.value.location.containerName
  var objectName = args.payload.value.location.objectName
  var callbackUrl = args.payload.value.callbackURL
  e2ee.encrypt(containerName, objectName, host, callbackUrl);
  res.end();
}

exports.openSessionPOST = function(args, host, res, next) {
  var username = "bla"
  var passphrase = args.payload.value.passphrase
  crypto.openSession(username, passphrase, host, function(){
    res.end();
  });
}

