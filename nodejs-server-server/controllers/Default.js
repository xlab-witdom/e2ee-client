'use strict';

var url = require('url');


var Default = require('./DefaultService');

function getRemoteIP(req) {
	var ip = req.connection.remoteAddress;
	//ip = ip.split(',')[0];
    ip = ip.split(':').slice(-1)[0];
    return ip
}

module.exports.decryptPOST = function decryptPOST (req, res, next) {
  Default.decryptPOST(req.swagger.params, getRemoteIP(req), res, next);
};

module.exports.encryptPOST = function encryptPOST (req, res, next) {
  Default.encryptPOST(req.swagger.params, getRemoteIP(req), res, next);
};

module.exports.openSessionPOST = function openSessionPOST (req, res, next) {
  Default.openSessionPOST(req.swagger.params, getRemoteIP(req), res, next);
};
