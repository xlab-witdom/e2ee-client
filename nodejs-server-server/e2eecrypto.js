var superagent = require("superagent");
var async = require("async");
var jsondiffpatch = require("jsondiffpatch");
var sjcl = require("./sjcl");
var e2ee = require("./e2ee");
var ui = require("./ui");
var BLAKE2s = require("blake2s");
var extend = require("extend");
var $ = require("jquery");
var nconf = require("nconf");

var loggers = require("./log");
var logger = loggers.logger;
var perfLogger = loggers.perfLogger;

// this is only for development phase - to allow self-signed certs !!!!!!!!!
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"


/* Crypton Client, Copyright 2013 SpiderOak, Inc.
 *
 * This file is part of Crypton Client.
 *
 * Crypton Client is free software: you can redistribute it and/or modify it
 * under the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Crypton Client is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the Affero GNU General Public
 * License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * along with Crypton Client.  If not, see <http://www.gnu.org/licenses/>.
 */

var crypton = {};

var tStartOpenSession;
var tOpen;
//module.exports=tStartOpenSession;

(function() {

    'use strict';

    var MISMATCH_ERR = 'Server and client version mismatch';

    /**!
     * ### version
     * Holds framework version for potential future backward compatibility.
     * '0.0.4' string is replaced with the version from package.json
     * at build time
     */
    crypton.version = '0.0.4';

    /**!
     * ### MIN_PBKDF2_ROUNDS
     * Minimum number of PBKDF2 rounds
     */
    crypton.MIN_PBKDF2_ROUNDS = 1000;

    /**!
     * ### clientVersionMismatch
     * Holds cleint <-> server version mismatch status
     */
    crypton.clientVersionMismatch = undefined;

    crypton.bearer = function(request) {
        request.set('Authorization', 'Bearer ' + crypton.token);
    };

    exports.openSession = function openSession(username, passphrase, host, callback) {
        console.log();
        perfLogger.log("info", "Received [openSession] from [", host, "]");
        var url = crypton.url() + '/accountexists';
        tStartOpenSession = new Date();
        superagent.get(url)
            //.use(crypton.bearer)
            .end(function(err, res) {
                if (err != null) {
                    logger.log("error", "Cannot connect to server - the reason might be a self signed certificate, in this case you can open E2EE server address in a browser and resolve a privacy error:", err);
                    perfLogger.log("info", "Elapsed [", new Date() - tStartOpenSession, "]. Process finished with status [failure]");
                }
                if (res.body.exists) {
                    var tAuthorize = new Date();
                    crypton.authorize(username, passphrase, function(err, session) {
                        perfLogger.log("info", "Elapsed [", new Date() - tAuthorize, "] for [authorize/E2EE-server]");
                        if (err) {
                            logger.log("error", err);
                            perfLogger.log("info", "Elapsed [", new Date() - tStartOpenSession, "]. Process finished with status [failure]");
                        } else {
                            tOpen = new Date();
                            e2ee.UI.open(username, session, function() {
                                perfLogger.log("info", "Elapsed [", new Date() - tOpen, "] for [doOpenSession/E2EE-server]");
                                perfLogger.log("info", "Elapsed [", new Date() - tStartOpenSession, "]. Process finished with status [success]");
                                callback()
                            })
                        }
                    })
                } else {
                    var tGenerateAccount = new Date();
                    crypton.generateAccount(username, passphrase, function done(err, account) {
                        perfLogger.log("info", "Elapsed [", new Date() - tGenerateAccount, "] for [generateCryptonAccount/E2EE-server]");
                        if (err) {
                            logger.log("error", err);
                            perfLogger.log("info", "Elapsed [", new Date() - tStartOpenSession, "]. Process finished with status [failure]");
                        } else {
                            logger.log("info", "Account created");
                            var tAuthorize = new Date();
                            crypton.authorize(username, passphrase, function(err, session) {
                                perfLogger.log("info", "Elapsed [", new Date() - tAuthorize, "] for [authorize/E2EE-server]");
                                if (err) {
                                    logger.log("error", err);
                                    perfLogger.log("info", "Elapsed [", new Date() - tStartOpenSession, "]. Process finished with status [failure]");
                                } else {
                                    tOpen = new Date();
                                    e2ee.UI.open(username, session, function() {
                                        perfLogger.log("info", "Elapsed [", new Date() - tOpen, "] for [doOpenSession/E2EE-server]");
                                        perfLogger.log("info", "Elapsed [", new Date() - tStartOpenSession, "]. Process finished with status [success]");
                                        callback();
                                    })
                                }
                            })
                        }
                    })
                }
            });
    };

    crypton.versionCheck = function(skip, callback) {
        if (skip) {
            return callback(null);
        }

        var url = crypton.url() + '/versioncheck?' + 'v=' + crypton.version;
        superagent.get(url)
            .use(crypton.bearer)
            .end(function(err, res) {

                if (res.body.success !== true && res.body.error !== undefined) {
                    crypton.clientVersionMismatch = true;
                    return callback(res.body.error);
                }
                callback(null);
            });
    };

    /**!
     * ### port
     * Holds port of Crypton server
     */
    crypton.port = 8080;

    /**!
     * ### cipherOptions
     * Sets AES mode to CCM to enable fast (based on ArrayBuffers) encryption/decryption
     */
    crypton.cipherOptions = {
        mode: 'ccm'
    };

    /**!
     * ### paranoia
     * Tells SJCL how strict to be about PRNG readiness
     */
    crypton.paranoia = 6;

    /**!
     * ### trustedPeers
     * Internal name for trusted peer (contacts list)
     */
    crypton.trustedPeers = '_trusted_peers';

    /**!
     * ### collectorsStarted
     * Internal flag to know if startCollectors has been called
     */
    crypton.collectorsStarted = false;

    /**!
     * ### startCollectors
     * Start sjcl.random listeners for adding to entropy pool
     */
    crypton.startCollectors = function() {
        sjcl.random.startCollectors();
        crypton.collectorsStarted = true;
    };

    /**!
     * ### url()
     * Generate URLs for server calls
     *
     * @return {String} url
     */
    crypton.url = function() {
	nconf.file({file: '../config.json'});
        return nconf.get('e2ee-server-address');
    };

    /**!
     * ### randomBytes(nbytes)
     * Generate `nbytes` bytes of random data
     *
     * @param {Number} nbytes
     * @return {Array} bitArray
     */
    function randomBytes(nbytes) {
        if (!nbytes) {
            throw new Error('randomBytes requires input');
        }

        if (parseInt(nbytes, 10) !== nbytes) {
            throw new Error('randomBytes requires integer input');
        }

        if (nbytes < 4) {
            throw new Error('randomBytes cannot return less than 4 bytes');
        }

        if (nbytes % 4 !== 0) {
            throw new Error('randomBytes requires input as multiple of 4');
        }

        // sjcl's words are 4 bytes (32 bits)
        var nwords = nbytes / 4;
        return sjcl.random.randomWords(nwords);
    }
    crypton.randomBytes = randomBytes;

    /**!
     * ### constEqual()
     * Compare two strings in constant time.
     *
     * @param {String} str1
     * @param {String} str2
     * @return {bool} equal
     */
    function constEqual(str1, str2) {
        // We only support string comparison, we could support Arrays but
        // they would need to be single char elements or compare multichar
        // elements constantly. Going for simplicity for now.
        // TODO: Consider this ^
        if (typeof str1 !== 'string' || typeof str2 !== 'string') {
            return false;
        }

        var mismatch = str1.length ^ str2.length;
        var len = Math.min(str1.length, str2.length);

        for (var i = 0; i < len; i++) {
            mismatch |= str1.charCodeAt(i) ^ str2.charCodeAt(i);
        }

        return mismatch === 0;
    }
    crypton.constEqual = constEqual;

    crypton.sessionId = null;

    /**!
     * ### randomBits(nbits)
     * Generate `nbits` bits of random data
     *
     * @param {Number} nbits
     * @return {Array} bitArray
     */
    crypton.randomBits = function(nbits) {
        if (!nbits) {
            throw new Error('randomBits requires input');
        }

        if (parseInt(nbits, 10) !== nbits) {
            throw new Error('randomBits requires integer input');
        }

        if (nbits < 32) {
            throw new Error('randomBits cannot return less than 32 bits');
        }

        if (nbits % 32 !== 0) {
            throw new Error('randomBits requires input as multiple of 32');
        }

        var nbytes = nbits / 8;
        return crypton.randomBytes(nbytes);
    };

    /**!
     * ### mac(key, data)
     * Generate an HMAC using `key` for `data`.
     *
     * @param {String} key
     * @param {String} data
     * @return {String} hmacHex
     */
    crypton.hmac = function(key, data) {
        var mac = new sjcl.misc.hmac(key);
        return sjcl.codec.hex.fromBits(mac.mac(data));
    }

    /**!
     * ### macAndCompare(key, data, otherMac)
     * Generate an HMAC using `key` for `data` and compare it in
     * constant time to `otherMac`.
     *
     * @param {String} key
     * @param {String} data
     * @param {String} otherMac
     * @return {Bool} compare succeeded
     */
    crypton.hmacAndCompare = function(key, data, otherMac) {
        var ourMac = crypton.hmac(key, data);
        return crypton.constEqual(ourMac, otherMac);
    };

    /**!
     * ### fingerprint(pubKey, signKeyPub)
     * Generate a fingerprint for an account or peer.
     *
     * @param {PublicKey} pubKey
     * @param {PublicKey} signKeyPub
     * @return {String} hash
     */
    // TODO check inputs
    crypton.fingerprint = function(pubKey, signKeyPub) {
        var pubKeys = sjcl.bitArray.concat(
            pubKey._point.toBits(),
            signKeyPub._point.toBits()
        );

        return crypton.hmac('', pubKeys);
    };

    /**!
     * ### generateAccount(username, passphrase, callback, options)
     * Generate salts and keys necessary for an account
     *
     * Saves account to server unless `options.save` is falsey
     *
     * Calls back with account and without error if successful
     *
     * Calls back with error if unsuccessful
     *
     * @param {String} username
     * @param {String} passphrase
     * @param {Function} callback
     * @param {Object} options
     */

    // TODO consider moving non-callback arguments to single object
    crypton.generateAccount = function(username, passphrase, callback, options) {
        if (crypton.clientVersionMismatch) {
            return callback(MISMATCH_ERR);
        }

        options = options || {};
        var save = typeof options.save !== 'undefined' ? options.save : true;

        crypton.versionCheck(!save, function(err) {
            if (err) {
                return callback(MISMATCH_ERR);
            } else {

                if (!passphrase) {
                    return callback('Must supply passphrase');
                }

                if (!crypton.collectorsStarted) {
                    // TODO
                    //crypton.startCollectors();
                }

                var SIGN_KEY_BIT_LENGTH = 384;
                var keypairCurve = options.keypairCurve || 384;
                var numRounds = crypton.MIN_PBKDF2_ROUNDS;

                var account = new crypton.Account();
                var hmacKey = randomBytes(32);
                var keypairSalt = randomBytes(32);
                var keypairMacSalt = randomBytes(32);
                var signKeyPrivateMacSalt = randomBytes(32);
                var containerNameHmacKey = randomBytes(32);
                var keypairKey = sjcl.misc.pbkdf2(passphrase, keypairSalt, numRounds);
                var keypairMacKey = sjcl.misc.pbkdf2(passphrase, keypairMacSalt, numRounds);
                var signKeyPrivateMacKey = sjcl.misc.pbkdf2(passphrase, signKeyPrivateMacSalt, numRounds);
                var keypair = sjcl.ecc.elGamal.generateKeys(keypairCurve, crypton.paranoia);
                var signingKeys = sjcl.ecc.ecdsa.generateKeys(SIGN_KEY_BIT_LENGTH, crypton.paranoia);

                account.username = username;
                account.keypairSalt = JSON.stringify(keypairSalt);
                account.keypairMacSalt = JSON.stringify(keypairMacSalt);
                account.signKeyPrivateMacSalt = JSON.stringify(signKeyPrivateMacSalt);

                // pubkeys
                account.pubKey = JSON.stringify(keypair.pub.serialize());
                account.signKeyPub = JSON.stringify(signingKeys.pub.serialize());

                var sessionIdentifier = 'dummySession';
                var session = new crypton.Session(sessionIdentifier);
                session.account = account;
                session.account.signKeyPrivate = signingKeys.sec;

                var selfPeer = new crypton.Peer({
                    session: session,
                    pubKey: keypair.pub,
                    signKeyPub: signingKeys.pub
                });
                selfPeer.trusted = true;

                // hmac keys
                var encryptedHmacKey = selfPeer.encryptAndSign(JSON.stringify(hmacKey));
                if (encryptedHmacKey.error) {
                    callback(encryptedHmacKey.error, null);
                    return;
                }

                account.hmacKeyCiphertext = JSON.stringify(encryptedHmacKey);

                var encryptedContainerNameHmacKey = selfPeer.encryptAndSign(JSON.stringify(containerNameHmacKey));
                if (encryptedContainerNameHmacKey.error) {
                    callback(encryptedContainerNameHmacKey.error, null);
                    return;
                }

                account.containerNameHmacKeyCiphertext = JSON.stringify(encryptedContainerNameHmacKey);

                // private keys
                // TODO: Check data auth with hmac
                var keypairCiphertext = sjcl.encrypt(keypairKey, JSON.stringify(keypair.sec.serialize()), crypton.cipherOptions);

                account.keypairCiphertext = keypairCiphertext;
                account.keypairMac = crypton.hmac(keypairMacKey, account.keypairCiphertext);
                account.signKeyPrivateCiphertext = sjcl.encrypt(keypairKey, JSON.stringify(signingKeys.sec.serialize()), crypton.cipherOptions);
                account.signKeyPrivateMac = crypton.hmac(signKeyPrivateMacKey, account.signKeyPrivateCiphertext);

                if (save) {
                    account.save(function(err) {
                        callback(err, account);
                    });
                    return;
                }

                callback(null, account);
            }
        });
    };

    /**!
     * ### authorize(username, passphrase, callback)
     * Perform zero-knowledge authorization with given `username`
     * and `passphrase`, generating a session if successful
     *
     * Calls back with session and without error if successful
     *
     * Calls back with error if unsuccessful
     *
     * @param {String} username
     * @param {String} passphrase
     * @param {Function} callback
     * @param {Object} options
     */
    crypton.authorize = function(username, passphrase, callback, options) {
        if (crypton.clientVersionMismatch) {
            return callback(MISMATCH_ERR);
        }

        options = options || {};
        var check = typeof options.check !== 'undefined' ? options.check : true;

        crypton.versionCheck(!check, function(err) {
            if (err) {
                return callback(MISMATCH_ERR);
            } else {

                if (!passphrase) {
                    return callback('Must supply passphrase');
                }

                if (!crypton.collectorsStarted) {
                    // TODO
                    //crypton.startCollectors();
                }

                var options = {
                    //username: username,
                    passphrase: passphrase
                };

                superagent.get(crypton.url() + '/account')
                    //.withCredentials()
                    //.send(response)
                    .use(crypton.bearer)
                    .end(function(err, res) {
                        if (!res.body || res.body.success !== true) {
                            return callback(res.body.error);
                        }

                        var session = new crypton.Session(crypton.sessionId);
                        session.account = new crypton.Account();
                        session.account.username = username;
                        session.account.passphrase = passphrase;
                        session.account.containerNameHmacKeyCiphertext = JSON.parse(res.body.account.containerNameHmacKeyCiphertext);
                        session.account.hmacKeyCiphertext = JSON.parse(res.body.account.hmacKeyCiphertext);
                        session.account.keypairCiphertext = res.body.account.keypairCiphertext;
                        session.account.keypairMac = res.body.account.keypairMac;
                        session.account.pubKey = JSON.parse(res.body.account.pubKey);
                        session.account.keypairSalt = JSON.parse(res.body.account.keypairSalt);
                        session.account.keypairMacSalt = JSON.parse(res.body.account.keypairMacSalt);
                        session.account.signKeyPub = sjcl.ecc.deserialize(JSON.parse(res.body.account.signKeyPub));
                        session.account.signKeyPrivateCiphertext = res.body.account.signKeyPrivateCiphertext;
                        session.account.signKeyPrivateMacSalt = JSON.parse(res.body.account.signKeyPrivateMacSalt);
                        session.account.signKeyPrivateMac = res.body.account.signKeyPrivateMac;
                        session.account.unravel(function(err) {
                            if (err) {
                                return callback(err);
                            }

                            session.load(crypton.trustedPeers, function(err, container) {
                                logger.log("info", "Loading trusted peers");
                                if (err) {
                                    logger.log("info", 'trustedPeers container does not exist - this is expected when user logins for the first time:', err);

                                    session.create(crypton.trustedPeers, function(err, peersContainer) {
                                        e2ee.session.peersContainer = peersContainer
                                        peersContainer.add('peers', function() {
                                            peersContainer.save(function(err) {
                                                if (err) {
                                                    logger.log("error", 'peers container could not be saved');
                                                } else {
                                                    callback(null, session);
                                                }
                                            })
                                        })
                                    })
                                } else {
                                    e2ee.session.peersContainer = container
                                    callback(null, session);
                                }
                            })
                        });
                    });
            }
        });
    };
})();


/* Crypton Client, Copyright 2013 SpiderOak, Inc.
 *
 * This file is part of Crypton Client.
 *
 * Crypton Client is free software: you can redistribute it and/or modify it
 * under the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Crypton Client is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the Affero GNU General Public
 * License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * along with Crypton Client.  If not, see <http://www.gnu.org/licenses/>.
 */

(function() {

    'use strict';

    /**!
     * # Account()
     *
     * ````
     * var account = new crypton.Account();
     * ````
     */
    var Account = crypton.Account = function Account() {};

    /**!
     * ### save(callback)
     * Send the current account to the server to be saved
     *
     * Calls back without error if successful
     *
     * Calls back with error if unsuccessful
     *
     * @param {Function} callback
     */
    Account.prototype.save = function(callback) {
        superagent.post(crypton.url() + '/account')
            //.withCredentials()
            .use(crypton.bearer)
            .send(this.serialize())
            //.send({"name":"New Account"})
            .end(function(err, res) {
                if (res.body.success !== true) {
                    callback(res.body.error);
                } else {
                    callback();
                }
            });
    };

    /**!
     * ### unravel(callback)
     * Decrypt raw account object from server after successful authentication
     *
     * Calls back without error if successful
     *
     * __Throws__ if unsuccessful
     *
     * @param {Function} callback
     */
    Account.prototype.unravel = function(callback) {
        var that = this;
        crypton.work.unravelAccount(this, function(err, data) {
            if (err) {
                return callback(err);
            }

            that.regenerateKeys(data, function(err) {
                callback(err);
            });
        });
    };

    /**!
     * ### regenerateKeys(callback)
     * Reconstruct keys from unraveled data
     *
     * Calls back without error if successful
     *
     * __Throws__ if unsuccessful
     *
     * @param {Function} callback
     */
    Account.prototype.regenerateKeys = function(data, callback) {
        // reconstruct secret key
        this.secretKey = sjcl.ecc.deserialize(data.secret);

        // reconstruct public key
        this.pubKey = sjcl.ecc.deserialize(this.pubKey);

        // assign the hmac keys to the account
        this.hmacKey = data.hmacKey;
        this.containerNameHmacKey = data.containerNameHmacKey;

        // reconstruct the public signing key - already reconstructed in authorize

        // reconstruct the secret signing key
        this.signKeyPrivate = sjcl.ecc.deserialize(data.signKeySecret);

        // calculate fingerprint for public key
        this.fingerprint = crypton.fingerprint(this.pubKey, this.signKeyPub);

        // recalculate the public points from secret exponents
        // and verify that they match what the server sent us
        var cP = this.secretKey._curve.G.mult(this.secretKey._exponent); // calculated point 
        var dP = this.pubKey.get(); // deserialized point

        if (!sjcl.bitArray.equal(cP.x.toBits(), dP.x) || !sjcl.bitArray.equal(cP.y.toBits(), dP.y)) {
            return callback('Server provided incorrect public key');
        }

        cP = this.signKeyPrivate._curve.G.mult(this.signKeyPrivate._exponent);
        dP = this.signKeyPub.get();
        if (!sjcl.bitArray.equal(cP.x.toBits(), dP.x) || !sjcl.bitArray.equal(cP.y.toBits(), dP.y)) {
            return callback('Server provided incorrect public signing key');
        }

        // sometimes the account object is used as a peer
        // to make the code simpler. verifyAndDecrypt checks
        // that the peer it is passed is trusted, or returns
        // an error. if we've gotten this far, we can be sure
        // that the public keys are trustable.
        this.trusted = true;

        callback(null);
    };

    /**!
     * ### serialize()
     * Package and return a JSON representation of the current account
     *
     * @return {Object}
     */
    // TODO rename to toJSON
    Account.prototype.serialize = function() {
        return {
            containerNameHmacKeyCiphertext: this.containerNameHmacKeyCiphertext,
            hmacKeyCiphertext: this.hmacKeyCiphertext,
            keypairCiphertext: this.keypairCiphertext,
            keypairMac: this.keypairMac,
            pubKey: this.pubKey,
            keypairSalt: this.keypairSalt,
            keypairMacSalt: this.keypairMacSalt,
            signKeyPrivateMacSalt: this.signKeyPrivateMacSalt,
            username: this.username,
            signKeyPub: this.signKeyPub,
            signKeyPrivateCiphertext: this.signKeyPrivateCiphertext,
            signKeyPrivateMac: this.signKeyPrivateMac
        };
    };

    /**!
     * ### verifyAndDecrypt()
     * Convienence function to verify and decrypt public key encrypted & signed data
     *
     * @return {Object}
     */
    Account.prototype.verifyAndDecrypt = function(signedCiphertext, peer) {
        if (!peer.trusted) {
            return {
                error: 'Peer is untrusted'
            }
        }

        // hash the ciphertext
        var ciphertextString = JSON.stringify(signedCiphertext.ciphertext);
        var hash = sjcl.hash.sha256.hash(ciphertextString);
        // verify the signature
        var verified = false;
        try {
            verified = peer.signKeyPub.verify(hash, signedCiphertext.signature);
        } catch (ex) {
            logger.log("error", ex);
            logger.log("error", ex.stack);
        }
        // try to decrypt regardless of verification failure
        try {
            var message = sjcl.decrypt(this.secretKey, ciphertextString, crypton.cipherOptions);
            if (verified) {
                return {
                    plaintext: message,
                    verified: verified,
                    error: null
                };
            } else {
                return {
                    plaintext: null,
                    verified: false,
                    error: 'Cannot verify ciphertext'
                };
            }
        } catch (ex) {
            logger.log("error", ex);
            logger.log("error", ex.stack);
            return {
                plaintext: null,
                verified: false,
                error: 'Cannot verify ciphertext'
            };
        }
    };

    /**!
     * ### changePassphrase()
     * Convienence function to change the user's passphrase
     *
     * @param {String} currentPassphrase
     * @param {String} newPassphrase
     * @param {Function} callback
     * callback will be handed arguments err, isComplete
     * Upon completion of a passphrase change, the client will be logged out
     * This callback should handle getting the user logged back in
     * programmatically or via the UI
     * @param {Function} keygenProgressCallback [optional]
     * @param {Boolean} skipCheck [optional]
     * @return void
     */
    Account.prototype.changePassphrase =
        function(currentPassphrase, newPassphrase,
            callback, keygenProgressCallback, skipCheck) {
            if (skipCheck) {
                if (currentPassphrase == newPassphrase) {
                    var err = 'New passphrase cannot be the same as current password';
                    return callback(err);
                }
            }

            if (keygenProgressCallback) {
                if (typeof keygenProgressCallback == 'function') {
                    keygenProgressCallback();
                }
            }

            var MIN_PBKDF2_ROUNDS = crypton.MIN_PBKDF2_ROUNDS;
            var that = this;
            var username = this.username;
            // authorize to make sure the user knows the correct passphrase
            crypton.authorize(username, currentPassphrase, function(err, newSession) {
                if (err) {
                    logger.log("error", err);
                    return callback(err);
                }
                // We have authorized, time to create the new keyring parts we
                // need to update the database

                var currentAccount = newSession.account;

                // Replace all salts with new ones
                var keypairSalt = crypton.randomBytes(32);
                var keypairMacSalt = crypton.randomBytes(32);
                var signKeyPrivateMacSalt = crypton.randomBytes(32);

                var keypairKey =
                    sjcl.misc.pbkdf2(newPassphrase, keypairSalt, MIN_PBKDF2_ROUNDS);

                var keypairMacKey =
                    sjcl.misc.pbkdf2(newPassphrase, keypairMacSalt, MIN_PBKDF2_ROUNDS);

                var signKeyPrivateMacKey =
                    sjcl.misc.pbkdf2(newPassphrase, signKeyPrivateMacSalt, MIN_PBKDF2_ROUNDS);

                var privateKeys = {
                    // 'privateKey/HMAC result name': serializedKey or string HMAC input data
                    containerNameHmacKeyCiphertext: currentAccount.containerNameHmacKey,
                    hmacKeyCiphertext: currentAccount.hmacKey,
                    signKeyPrivateCiphertext: currentAccount.signKeyPrivate.serialize(),
                    keypairCiphertext: currentAccount.secretKey.serialize(),
                    keypairMacKey: keypairMacKey,
                    signKeyPrivateMacKey: signKeyPrivateMacKey
                };

                var newKeyring;

                try {
                    newKeyring = that.wrapAllKeys(keypairKey, privateKeys, newSession);
                } catch (ex) {
                    logger.log("error", ex);
                    logger.log("error", ex.stack);
                    return callback('Fatal: cannot wrap keys, see error console for more information');
                }

                // Set other new properties before we save
                newKeyring.keypairSalt = JSON.stringify(keypairSalt);
                newKeyring.keypairMacSalt = JSON.stringify(keypairMacSalt);
                newKeyring.signKeyPrivateMacSalt = JSON.stringify(signKeyPrivateMacSalt);
                newKeyring.srpVerifier = srpVerifier;
                newKeyring.srpSalt = srpSalt;
                var url = crypton.url() + '/account/' + that.username + '/keyring?sid=' + crypton.sessionId;
                superagent.post(url)
                    .withCredentials()
                    .send(newKeyring)
                    .end(function(res) {
                        if (res.body.success !== true) {
                            logger.log('error', res.body.error);
                            callback(res.body.error);
                        } else {
                            // XXX TODO: Invalidate all other client sessions before doing:
                            newSession = null; // Force new login after passphrase change
                            callback(null, true); // Do not hand the new session to the callback
                        }
                    });

            }, null);
        };

    /**!
     * ### wrapKey()
     * Helper function to wrap keys
     *
     * @param {String} selfPeer
     * @param {String} serializedPrivateKey
     * @return {Object} wrappedKey
     */
    Account.prototype.wrapKey = function(selfPeer, serializedPrivateKey) {
        if (!selfPeer || !serializedPrivateKey) {
            throw new Error('selfPeer and serializedPrivateKey are required');
        }
        var serializedKey;
        if (typeof serializedPrivateKey != 'string') {
            serializedKey = JSON.stringify(serializedPrivateKey);
        } else {
            serializedKey = serializedPrivateKey;
        }
        var wrappedKey = selfPeer.encryptAndSign(serializedKey);
        if (wrappedKey.error) {
            return null;
        }
        return wrappedKey;
    };

    /**!
     * ### wrapAllKeys()
     * Helper function to wrap all keys when changing passphrase, etc
     *
     * @param {String} wrappingKey
     * @param {Object} privateKeys
     * @param {Object} Session
     * @return {Object} wrappedKey
     */
    Account.prototype.wrapAllKeys = function(wrappingKey, privateKeys, session) {
        // Using the *labels* of the future wrapped objects here
        var requiredKeys = [
            'containerNameHmacKeyCiphertext',
            'hmacKeyCiphertext',
            'signKeyPrivateCiphertext',
            'keypairCiphertext', // main encryption private key
            'keypairMacKey',
            'signKeyPrivateMacKey'
        ];

        var privateKeysLength = Object.keys(privateKeys).length;
        var privateKeyNames = Object.keys(privateKeys);

        for (var i = 0; i < privateKeysLength; i++) {
            var keyName = privateKeyNames[i];
            if (requiredKeys.indexOf(keyName) == -1) {
                throw new Error('Missing private key: ' + keyName);
            }
        }
        // Check that the length of privateKeys is correct
        if (privateKeysLength != requiredKeys.length) {
            throw new Error('privateKeys length does not match requiredKeys length');
        }

        var selfPeer = new crypton.Peer({
            session: session,
            pubKey: session.account.pubKey,
            signKeyPub: session.account.signKeyPub
        });
        selfPeer.trusted = true;

        var result = {};

        var hmacKeyCiphertext = this.wrapKey(selfPeer,
            privateKeys.hmacKeyCiphertext);
        if (hmacKeyCiphertext.error) {
            result.hmacKeyCiphertext = null;
        } else {
            result.hmacKeyCiphertext = JSON.stringify(hmacKeyCiphertext);
        }

        var containerNameHmacKeyCiphertext =
            this.wrapKey(selfPeer,
                privateKeys.containerNameHmacKeyCiphertext);

        if (containerNameHmacKeyCiphertext.error) {
            result.containerNameHmacKeyCiphertext = null;
        } else {
            result.containerNameHmacKeyCiphertext = JSON.stringify(containerNameHmacKeyCiphertext);
        }

        // Private Keys
        var keypairCiphertext =
            sjcl.encrypt(wrappingKey,
                JSON.stringify(privateKeys.keypairCiphertext),
                crypton.cipherOptions);

        if (keypairCiphertext.error) {
            logger.log("error",keypairCiphertext.error);
            keypairCiphertext = null;
        }
        result.keypairCiphertext = keypairCiphertext;

        var signKeyPrivateCiphertext =
            sjcl.encrypt(wrappingKey, JSON.stringify(privateKeys.signKeyPrivateCiphertext),
                crypton.cipherOptions);

        if (signKeyPrivateCiphertext.error) {
            logger.log("error", signKeyPrivateCiphertext.error);
            signKeyPrivateCiphertext = null;
        }
        result.signKeyPrivateCiphertext = signKeyPrivateCiphertext;

        // HMACs
        result.keypairMac =
            crypton.hmac(privateKeys.keypairMacKey, result.keypairCiphertext);

        result.signKeyPrivateMac = crypton.hmac(privateKeys.signKeyPrivateMacKey,
            result.signKeyPrivateCiphertext);
        for (var keyName in result) {
            if (!result[keyName]) {
                throw new Error('Fatal: ' + keyName + ' is null');
            }
        }
        return result;
    };

})();

/* Crypton Client, Copyright 2013 SpiderOak, Inc.
 *
 * This file is part of Crypton Client.
 *
 * Crypton Client is free software: you can redistribute it and/or modify it
 * under the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Crypton Client is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the Affero GNU General Public
 * License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * along with Crypton Client.  If not, see <http://www.gnu.org/licenses/>.
 */

(function() {

    'use strict';

    var ERRS;

    /**!
     * # Session(id)
     *
     * ````
     * var session = new crypton.Session(id);
     * ````
     *
     * @param {Number} id
     */
    var Session = crypton.Session = function(id) {
        ERRS = crypton.errors;
        this.id = id;
        this.peers = {};
        this.events = {};
        this.containers = [];
        this.items = {};
        /*var arrX = [216641276, 1692448605, -1924501857, 1827387796, 779748572, 843150245, 1099493403, 
 	 1059976798, -180817969, -67584009, -1813773813, -191652818];
  var arrY = [563868699, -138412012, 966188095, -1677562130, 804621771, 907353981, -803448850, 
 	 -214138144, 1386665954, -1492810573, 1174706855, -995587158];
  var curve = sjcl.ecc.curves['c384'];
  var pubkey_x = new curve.field(sjcl.bn.fromBits(arrX));
  var pubkey_y = new curve.field(sjcl.bn.fromBits(arrY));
  var point = new sjcl.ecc.point(curve, pubkey_x, pubkey_y);
  this.serverPubSignatureKey = new sjcl.ecc.ecdsa.publicKey(curve, point);
  */

        var curve = sjcl.ecc.curves['c384'];
        var point = [216641276, 1692448605, -1924501857, 1827387796, 779748572, 843150245, 1099493403, 1059976798, -180817969, -67584009, -1813773813, -191652818, 563868699, -138412012, 966188095, -1677562130, 804621771, 907353981, -803448850, -214138144, 1386665954, -1492810573, 1174706855, -995587158];
        this.serverPubSignatureKey = new sjcl.ecc.ecdsa.publicKey(curve, point);

        var that = this;

        /*
        var joinServerParameters = { token: crypton.sessionId };
        this.socket = io.connect(crypton.url(),
                                 { query: 'joinServerParameters='
                                        + JSON.stringify(joinServerParameters),
                                   reconnection: true,
                                   reconnectionDelay: 5000
                                 });
        // watch for incoming Inbox messages
        this.socket.on('message', function (data) {
          that.inbox.get(data.messageId, function (err, message) {
            that.emit('message', message);
          });
        });
        // watch for container update notifications
        this.socket.on('containerUpdate', function (containerNameHmac) {
          // if any of the cached containers match the HMAC
          // in the notification, sync the container and
          // call the listener if one has been set
          for (var i = 0; i < that.containers.length; i++) {
            var container = that.containers[i];
            var temporaryHmac = container.containerNameHmac || container.getPublicName();
            if (crypton.constEqual(temporaryHmac, containerNameHmac)) {
              container.sync(function (err) {
                if (container._listener) {
                  container._listener();
                }
              });
              break;
            }
          }
        });
        // watch for Item update notifications
        this.socket.on('itemUpdate', function (itemObj) {
          if (!itemObj.itemNameHmac || !itemObj.creator || !itemObj.toUsername) {
            console.error(ERRS.ARG_MISSING);
            throw new Error(ERRS.ARG_MISSING);
          }
          console.log('Item updated!', itemObj);
          // if any of the cached items match the HMAC
          // in the notification, sync the items and
          // call the listener if one has been set
          if (that.items[itemObj.itemNameHmac]) {
            that.items[itemObj.itemNameHmac].sync(function (err) {
              if (err) {
                return console.error(err);
              }
              try {
                that.events.onSharedItemSync(that.items[itemObj.itemNameHmac]);
              } catch (ex) {
                console.warn(ex);
              }
              if (that.items[itemObj.itemNameHmac]._listener) {
                that.items[itemObj.itemNameHmac]._listener(err);
              }
            });
          } else {
            console.log('Loading the item as it is not cached');
            // load item!
            // get the peer first:
            that.getPeer(itemObj.creator, function (err, peer) {
              if (err) {
                console.error(err);
                console.error('Cannot load item: creator peer cannot be found');
                return;
              }
              // XXXddahl: Make sure you trust this peer before loading the item
              //           Perhaps we check this inside the Item constructor?
              var itemCallback = function _itemCallback (err, item) {
                if (err) {
                  console.error(err);
                  return;
                }
                that.items[itemObj.itemNameHmac] = item;
                try {
                  that.events.onSharedItemSync(item);
                } catch (ex) {
                  console.warn(ex);
                }
              };
              var item =
                new crypton.Item(null, null, that, peer,
                                 itemCallback, itemObj.itemNameHmac);
            });
          }
        });
        */
    };

    /**!
     * ### removeItem(itemNameHmac, callback)
     * Remove/delete Item with given 'itemNameHmac',
     * both from local cache & server
     *
     * Calls back with success boolean and without error if successful
     *
     * Calls back with error if unsuccessful
     *
     * @param {String} itemNameHmac
     * @param {Function} callback
     */
    Session.prototype.removeItem = function removeItem(itemNameHmac, callback) {
        var that = this;
        for (var name in this.items) {
            if (this.items[name].nameHmac == itemNameHmac) {
                this.items[name].remove(function(err) {
                    if (err) {
                        logger.log("error", err);
                        callback(err);
                        return;
                    }
                    if (that.items[name].deleted) {
                        delete that.items[name];
                        callback(null);
                    }
                });
            }
        }
    };

    /**!
     * ### getOrCreateItem(itemName, callback)
     * Create or Retrieve Item with given platintext `itemName`,
     * either from local cache or server
     *
     * This method is for use by the creator of the item.
     * Use 'session.getSharedItem' for items shared by the creator
     *
     * Calls back with Item and without error if successful
     *
     * Calls back with error if unsuccessful
     *
     * @param {String} itemName
     * @param {Function} callback
     */
    Session.prototype.getOrCreateItem =
        function getOrCreateItem(itemName, callback) {

            if (!itemName) {
                return callback('itemName is required');
            }
            if (!callback) {
                throw new Error('Missing required callback argument');
            }
            // Get cached item if exists
            // XXXddahl: check server for more recent item?
            // We need another server API like /itemupdated/<itemHmacName> which returns
            // the timestamp of the last update
            if (this.items[itemName]) {
                callback(null, this.items[itemName]);
                return;
            }

            var creator = this.createSelfPeer();
            var item =
                new crypton.Item(itemName, null, this, creator, function getItemCallback(err, item) {
                    if (err) {
                        logger.log("error", err);
                        return callback(err);
                    }
                    callback(null, item);
                });
        };

    /**!
     * ### getSharedItem(itemNameHmac, peer, callback)
     * Retrieve shared Item with given itemNameHmac,
     * either from local cache or server
     *
     * Calls back with Item and without error if successful
     *
     * Calls back with error if unsuccessful
     *
     * @param {String} itemNameHmac
     * @param {Object} peer
     * @param {Function} callback
     */
    Session.prototype.getSharedItem =
        function getSharedItem(itemNameHmac, peer, callback) {
            // TODO:  Does not check for cached item or server having a fresher Item
            if (!itemNameHmac) {
                return callback(ERRS.ARG_MISSING);
            }
            if (!callback) {
                throw new Error(ERRS.ARG_MISSING_CALLBACK);
            }

            function getItemCallback(err, item) {
                if (err) {
                    logger.log("error", err);
                    return callback(err);
                }
                callback(null, item);
            }

            new crypton.Item(null, null, this, peer, getItemCallback, itemNameHmac);
        };

    /**!
     * ### createSelfPeer()
     * returns a 'selfPeer' object which is needed for any kind of
     * self-signing, encryption or verification
     *
     */
    Session.prototype.createSelfPeer = function() {
        var selfPeer = new crypton.Peer({
            session: this,
            pubKey: this.account.pubKey,
            signKeyPub: this.account.signKeyPub,
            signKeyPrivate: this.account.signKeyPrivate,
            username: this.account.username
        });
        selfPeer.trusted = true;
        return selfPeer;
    };

    /**!
     * ### load(containerName, callback)
     * Retieve container with given platintext `containerName`,
     * either from local cache or server
     *
     * Calls back with container and without error if successful
     *
     * Calls back with error if unsuccessful
     *
     * @param {String} containerName
     * @param {Function} callback
     */
    Session.prototype.load = function(containerName, callback) {
        // check for a locally stored container
        for (var i = 0; i < this.containers.length; i++) {
            if (crypton.constEqual(this.containers[i].name, containerName)) {
                callback(null, this.containers[i]);
                return;
            }
        }

        // check for a container on the server
        var that = this;
        this.getContainer(containerName, function(err, container) {
            if (err) {
                callback(err);
                return;
            }

            that.containers.push(container);
            callback(null, container);
        });
    };

    /**!
     * ### loadWithHmac(containerNameHmac, callback)
     * Retieve container with given `containerNameHmac`,
     * either from local cache or server
     *
     * Calls back with container and without error if successful
     *
     * Calls back with error if unsuccessful
     *
     * @param {String} containerNameHmac
     * @param {Function} callback
     */
    Session.prototype.loadWithHmac = function(containerNameHmac, peer, callback) {
        // check for a locally stored container
        for (var i = 0; i < this.containers.length; i++) {
            if (crypton.constEqual(this.containers[i].nameHmac, containerNameHmac)) {
                callback(null, this.containers[i]);
                return;
            }
        }

        // check for a container on the server
        var that = this;
        this.getContainerWithHmac(containerNameHmac, peer, function(err, container) {
            if (err) {
                callback(err);
                return;
            }

            that.containers.push(container);
            callback(null, container);
        });
    };

    /**!
     * ### create(containerName, callback)
     * Create container with given platintext `containerName`,
     * save it to server
     *
     * Calls back with container and without error if successful
     *
     * Calls back with error if unsuccessful
     *
     * @param {String} containerName
     * @param {Function} callback
     */
    Session.prototype.create = function(containerName, callback) {
        for (var i in this.containers) {
            if (crypton.constEqual(this.containers[i].name, containerName)) {
                callback('Container already exists');
                return;
            }
        }

        var selfPeer = new crypton.Peer({
            session: this,
            pubKey: this.account.pubKey,
            signKeyPub: this.account.signKeyPub
        });
        selfPeer.trusted = true;

        var sessionKey = crypton.randomBytes(32);
        var sessionKeyCiphertext = selfPeer.encryptAndSign(sessionKey);

        if (sessionKeyCiphertext.error) {
            return callback(sessionKeyCiphertext.error);
        }

        delete sessionKeyCiphertext.error;

        // TODO is signing the sessionKey even necessary if we're
        // signing the sessionKeyShare? what could the container
        // creator attack by wrapping a different sessionKey?
        var containerNameHmac = new sjcl.misc.hmac(this.account.containerNameHmacKey);
        containerNameHmac = sjcl.codec.hex.fromBits(containerNameHmac.encrypt(containerName));
        var chunk = {
            toAccount: this.account.username,
            sessionKeyCiphertext: JSON.stringify(sessionKeyCiphertext),
        };

        var url = crypton.url() + '/container/' + containerNameHmac;
        var that = this;
        superagent.put(url)
            //.withCredentials()
            .send(chunk)
            .use(crypton.bearer)
            .end(function(err, res) {
                if (!res.body || res.body.success !== true) {
                    callback(res.body.error);
                    return;
                }

                var container = new crypton.Container(that);
                container.name = containerName;
                container.sessionKey = sessionKey;
                that.containers.push(container);
                callback(null, container);
            });
    };

    /**!
     * ### deleteContainer(containerName, callback)
     * Request the server to delete all records and keys
     * belonging to `containerName`
     *
     * Calls back without error if successful
     *
     * Calls back with error if unsuccessful
     *
     * @param {String} containerName
     * @param {Function} callback
     */
    Session.prototype.deleteContainer = function(containerName, callback) {
        var that = this;
        var containerNameHmac = new sjcl.misc.hmac(this.account.containerNameHmacKey);
        containerNameHmac = sjcl.codec.hex.fromBits(containerNameHmac.encrypt(containerName));
        var url = crypton.url() + '/container/' + containerNameHmac;
        superagent.del(url)
            .use(crypton.bearer)
            .end(function(err, res) {
                if (res.body.success !== true && res.body.error !== undefined) {
                    return callback(res.body.error);
                }
                // remove from cache
                for (var i = 0; i < that.containers.length; i++) {
                    if (crypton.constEqual(that.containers[i].name, containerName)) {
                        that.containers.splice(i, 1);
                        break;
                    }
                }

                callback(null);
            });
    };

    /**!
     * ### getContainer(containerName, callback)
     * Retrieve container with given platintext `containerName`
     * specifically from the server
     *
     * Calls back with container and without error if successful
     *
     * Calls back with error if unsuccessful
     *
     * @param {String} containerName
     * @param {Function} callback
     */
    Session.prototype.getContainer = function(containerName, callback) {
        var container = new crypton.Container(this);
        container.name = containerName;
        container.sync(function(err) {
            callback(err, container);
        });
    };

    /**!
     * ### getContainerWithHmac(containerNameHmac, callback)
     * Retrieve container with given `containerNameHmac`
     * specifically from the server
     *
     * Calls back with container and without error if successful
     *
     * Calls back with error if unsuccessful
     *
     * @param {String} containerNameHmac
     * @param {Function} callback
     */
    Session.prototype.getContainerWithHmac = function(containerNameHmac, peer, callback) {
        var container = new crypton.Container(this);
        container.nameHmac = containerNameHmac;
        container.peer = peer;
        container.sync(function(err) {
            callback(err, container);
        });
    };

    /**!
     * ### getPeer(containerName, callback)
     * Retrieve a peer object from the database for given `username`
     *
     * Calls back with peer and without error if successful
     *
     * Calls back with error if unsuccessful
     *
     * @param {String} username
     * @param {Function} callback
     */
    Session.prototype.getPeer = function(username, callback) {
        if (this.peers[username]) {
            return callback(null, this.peers[username]);
        }

        var that = this;
        var peer = new crypton.Peer();
        peer.username = username;
        peer.session = this;

        peer.fetch(function(err, peer) {
            if (peer === undefined) {
                return callback("peer not registered");
            }
            if (err) {
                return callback(err);
            }

            e2ee.session.peersContainer.get('peers', function(err, peers) {
                if (err) {
                    logger.log("error", "Peers from peersContainer could not be retrieved:", err);
                    callback(err)
                } else {
                    if (!peers[username]) {
                        peer.trusted = false;
                    } else {
                        var savedFingerprint = peers[username].fingerprint;
                        if (!crypton.constEqual(savedFingerprint, peer.fingerprint)) {
                            return callback('Server has provided malformed peer', peer);
                        }
                        peer.trusted = true;
                    }

                    that.peers[username] = peer;
                    callback(null, peer);
                }
            })
        });
    };

    Session.prototype.getMessages = function(callback) {
        var that = this;
        var url = crypton.url() + '/messages';
        var messages = []
        superagent.get(url)
            .send(this.encrypted)
            .use(crypton.bearer)
            //.withCredentials()
            .end(function(err, res) {
                if (!res.body || res.body.success !== true) {
                    callback(res.body.error);
                    return;
                }

                //callback(null, res.body.messages);
                async.each(res.body.messages, function(rawMessage, callback) {
                        var message = new crypton.Message(that, rawMessage);
                        message.decrypt(function(err) {
                            messages.push(message);
                            callback();
                        });
                    },
                    function(err) {
                        if (callback) {
                            callback(null, messages);
                        }
                    })
            });
    };

    Session.prototype.deleteMessages = function(callback) {
        var that = this;
        var url = crypton.url() + '/messages';
        var messages = {}
        superagent.del(url)
            .send(this.encrypted)
            .use(crypton.bearer)
            //.withCredentials()
            .end(function(err, res) {
                if (!res.body || res.body.success !== true) {
                    callback(res.body.error);
                    return;
                }

                callback(null);
            });
    };


    /**!
     * ### on(eventName, listener)
     * Set `listener` to be called anytime `eventName` is emitted
     *
     * @param {String} eventName
     * @param {Function} listener
     */
    // TODO allow multiple listeners
    Session.prototype.on = function(eventName, listener) {
        this.events[eventName] = listener;
    };

    /**!
     * ### emit(eventName, data)
     * Call listener for `eventName`, passing it `data` as an argument
     *
     * @param {String} eventName
     * @param {Object} data
     */
    // TODO allow multiple listeners
    Session.prototype.emit = function(eventName, data) {
        this.events[eventName] && this.events[eventName](data);
    };

})();
/* Crypton Client, Copyright 2013 SpiderOak, Inc.
 *
 * This file is part of Crypton Client.
 *
 * Crypton Client is free software: you can redistribute it and/or modify it
 * under the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Crypton Client is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the Affero GNU General Public
 * License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * along with Crypton Client.  If not, see <http://www.gnu.org/licenses/>.
 */

(function() {

    'use strict';

    /**!
     * # Container(session)
     *
     * ````
     * var container = new crypton.Container(session);
     * ````
     *
     * @param {Object} session
     */
    var Container = crypton.Container = function(session) {
        this.keys = {};
        this.session = session;
        this.recordCount = 1;
        this.recordIndex = 0;
        this.versions = {};
        //this.version = +new Date();
        this.version = 0;
        this.name = null;
    };

    /**!
     * ### add(key, callback)
     * Add given `key` to the container
     *
     * Calls back without error if successful
     *
     * Calls back with error if unsuccessful
     *
     * @param {String} key
     * @param {Function} callback
     */
    Container.prototype.add = function(key, callback) {
        if (this.keys[key]) {
            callback('Key already exists');
            return;
        }

        this.keys[key] = {};
        callback();
    };

    /**!
     * ### get(key, callback)
     * Retrieve value for given `key`
     *
     * Calls back with `value` and without error if successful
     *
     * Calls back with error if unsuccessful
     *
     * @param {String} key
     * @param {Function} callback
     */
    Container.prototype.get = function(key, callback) {
        if (!this.keys[key]) {
            callback('Key does not exist');
            return;
        }

        callback(null, this.keys[key]);
    };

    /**!
     * ### save(callback, options)
     * Get difference of container since last save (a record),
     * encrypt the record, and send it to the server to be saved
     *
     * Calls back without error if successful
     *
     * Calls back with error if unsuccessful
     *
     * @param {Function} callback
     * @param {Object} options (optional)
     */
    Container.prototype.save = function(callback, options) {
        var that = this;

        this.getDiff(function(err, diff) {
            if (!diff) {
                callback('File has not changed');
                return;
            }

            var payload = {
                recordIndex: that.recordCount,
                delta: diff
            };

            var now = +new Date();
            var copiedObject = extend(true, {}, that.keys) // instead of: JSON.parse(JSON.stringify(that.keys))
            that.versions[now] = copiedObject;
            that.version = now;
            that.recordCount++;

            var rawPayloadCiphertext;
            var encryptAsArrayBuffer = false;
            if (payload.delta.chunks) {
                encryptAsArrayBuffer = true;
                // todo: check decryption
                for (key in payload.delta.chunks[0]) {
                    if (payload.delta.chunks[0].hasOwnProperty(key)) {
                        var value = payload.delta.chunks[0][key];
                        if (!(value instanceof Uint8Array)) {
                            // when the whole file is encrypted, payload.delta contains ArrayBuffer,
                            // however for futher changes payload.delta does not contain ArrayBuffer
                            encryptAsArrayBuffer = false;
                        }
                        break;
                    }
                }
            }
            if (encryptAsArrayBuffer) {
                // flatten the Uint8Array that was retrieved in chunks from file
                var chunksNum = Object.keys(payload.delta.chunks[0]).length
                var size = 0;
                for (var key in payload.delta.chunks[0]) {
                    var value = payload.delta.chunks[0][key];
                    size += value.length;
                }

                // Not the whole container is converted into Uint8Array - 
                // only chunks and metadata at the moment.
                // Note that metadata could be added to another container,
                // but than when sharing this container would be needed 
                // to be shared too ...
                var meta = JSON.stringify(payload.delta.metadata[0]);
                var buf = new ArrayBuffer(meta.length);
                var bufView = new Uint8Array(buf);
                for (var i = 0; i < meta.length; i++) {
                    bufView[i] = meta.charCodeAt(i);
                }
                var separationOffset = 10
                size += meta.length;
                size += separationOffset;

                var newArray = new Uint8Array(size);
                Object.keys(payload.delta.chunks[0]).forEach(function(key) { // keys are data positions
                    newArray.set(payload.delta.chunks[0][key], parseInt(key));
                });

                var tmpLen = newArray.length;
                newArray.set(bufView, size - meta.length);

                rawPayloadCiphertext = sjcl.encrypt(that.sessionKey, newArray.buffer, crypton.cipherOptions);
            } else {
                rawPayloadCiphertext = sjcl.encrypt(that.sessionKey, JSON.stringify(payload), crypton.cipherOptions);
            }

            var bytes = [];
            var str = JSON.stringify(rawPayloadCiphertext);
            for (var i = 0; i < str.length; ++i) {
                bytes.push(str.charCodeAt(i));
            }

            // hashing is really slow for large ciphertexts
            var b = new BLAKE2s(32);
            b.update(bytes);
            var payloadCiphertextHash = b.digest("hex");

            //var payloadCiphertextHash = sjcl.hash.sha256.hash(JSON.stringify(rawPayloadCiphertext));
            var payloadSignature = that.session.account.signKeyPrivate.sign(payloadCiphertextHash, crypton.paranoia);

            var payloadCiphertext = {
                ciphertext: rawPayloadCiphertext,
                signature: payloadSignature
            };

            var chunk = {
                containerNameHmac: that.getPublicName(),
                payloadCiphertext: JSON.stringify(payloadCiphertext)
            };

            // if we aren't saving it, we're probably testing
            // to see if the transaction chunk was generated correctly
            if (options && options.save == false) {
                callback(null, chunk);
                return;
            }

            var url = crypton.url() + '/container/record';
            superagent.post(url)
                //.withCredentials()
                .send(chunk)
                .use(crypton.bearer)
                .end(function(err, res) {
                    if (!res.body || res.body.success !== true) {
                        callback(res.body.error);
                        return;
                    }
                    callback(null);
                });
        });
    };

    /**!
     * ### getDiff(callback, options)
     * Compute difference of container since last save
     *
     * Calls back with diff object and without error if successful
     *
     * Calls back with error if unsuccessful
     *
     * @param {Function} callback
     */
    Container.prototype.getDiff = function(callback) {
        var last = this.latestVersion();
        var old = this.versions[last] || {};
        callback(null, crypton.diff.create(old, this.keys));
    };

    /**!
     * ### getVersions()
     * Return a list of known save point timestamps
     *
     * @return {Array} timestamps
     */
    Container.prototype.getVersions = function() {
        return Object.keys(this.versions);
    };

    /**!
     * ### getVersion(version)
     * Return full state of container at given `timestamp`
     *
     * @param {Number} timestamp
     * @return {Object} version
     */
    Container.prototype.getVersion = function(timestamp) {
        return this.versions[timestamp];
    };

    /**!
     * ### getVersion()
     * Return last known save point timestamp
     *
     * @return {Number} version
     */
    Container.prototype.latestVersion = function() {
        var versions = this.getVersions();

        if (!versions.length) {
            return this.version;
        } else {
            return Math.max.apply(Math, versions);
        }
    };

    /**!
     * ### getPublicName()
     * Compute the HMAC for the given name of the container
     *
     * @return {String} hmac
     */
    Container.prototype.getPublicName = function() {
        if (this.nameHmac) {
            return this.nameHmac;
        }

        var hmac = new sjcl.misc.hmac(this.session.account.containerNameHmacKey);
        var containerNameHmac = hmac.encrypt(this.name);
        this.nameHmac = sjcl.codec.hex.fromBits(containerNameHmac);
        return this.nameHmac;
    };

    /**!
     * ### getHistory()
     * Ask the server for all state records
     *
     * Calls back with diff object and without error if successful
     *
     * Calls back with error if unsuccessful
     *
     * @param {Function} callback
     */
    Container.prototype.getHistory = function(callback) {
        var that = this;
        var containerNameHmac = this.getPublicName();
        var currentVersion = this.latestVersion();

        var nonce = sjcl.codec.hex.fromBits(crypton.randomBytes(32));
        var url = crypton.url() + '/container/' + containerNameHmac + '?after=' + (currentVersion + 1) + '&nonce=' + nonce;

        logger.log("debug", "getHistory", url);
        superagent.get(url)
            //.withCredentials()
            // .set('X-Session-ID', crypton.sessionId)
            .use(crypton.bearer)
            .end(function(err, res) {
                if (!res.body || res.body.success !== true) {
                    callback(res.body.error);
                    return;
                }

                callback(null, res.body.records);
            });
    };

    /**!
     * ### parseHistory(records, callback)
     * Loop through given `records`, decrypt them,
     * and build object state from decrypted diff objects
     *
     * Calls back with full container state,
     * history versions, last record index,
     * and without error if successful
     *
     * Calls back with error if unsuccessful
     *
     * @param {Array} records
     * @param {Function} callback
     */
    Container.prototype.parseHistory = function(records, callback) {
        var that = this;
        var keys = that.keys || {};
        var versions = that.versions || {};

        var recordIndex = that.recordIndex + 1;

        async.eachSeries(records, function(rawRecord, callback) {
            that.decryptRecord(recordIndex, rawRecord, function(err, record) {
                if (err) {
                    return callback(err);
                }

                // TODO put in worker
                keys = crypton.diff.apply(record.delta, keys);

                var copiedObject = extend(true, {}, keys) // instead of: JSON.parse(JSON.stringify(keys))
                versions[record.time] = copiedObject;

                callback(null);
            });
        }, function(err) {
            if (err) {
                logger.log("error", 'Hit error parsing container history');
                logger.log("debug", that);
                logger.log("debug", err);

                return callback(err);
            }

            that.recordIndex = recordIndex;
            callback(null, keys, versions, recordIndex);
        });
    };

    /**!
     * ### decryptRecord(recordIndex, record, callback)
     * Decrypt record ciphertext with session key,
     * verify record index
     *
     * Calls back with object containing timestamp and delta
     * and without error if successful
     *
     * Calls back with error if unsuccessful
     *
     * @param {Object} recordIndex
     * @param {Object} record
     * @param {Object} callback
     */
    Container.prototype.decryptRecord = function(recordIndex, record, callback) {
        if (!this.sessionKey) {
            this.decryptKey(record);
        }

        var parsedRecord;
        try {
            parsedRecord = JSON.parse(record.payloadCiphertext);
        } catch (e) {}

        if (!parsedRecord) {
            return callback('Could not parse record JSON');
        }

        var options = {
            sessionKey: this.sessionKey,
            expectedRecordIndex: recordIndex,
            record: record.payloadCiphertext,
            creationTime: record.CreatedAt, // started with upper case because default gorm.Model is used on server side
            // we can't just send the peer object or its signKeyPub
            // here because of circular JSON when dealing with workers.
            // we'll have to reconstruct the signkey on the other end.
            // better to be explicit anyway!
            peerSignKeyPubSerialized: (
                this.peer && this.peer.signKeyPub || this.session.account.signKeyPub
            ).serialize()
        };

        crypton.work.decryptRecord(options, callback);
    };

    /**!
     * ### decryptKey(record)
     * Extract and decrypt the container's keys from a given record
     *
     * @param {Object} record
     */
    Container.prototype.decryptKey = function(record) {
        var peer = this.peer || this.session.account;
        var sessionKeyRaw = this.session.account.verifyAndDecrypt(JSON.parse(record.sessionKeyCiphertext), peer);

        if (sessionKeyRaw.error) {
            throw new Error(sessionKeyRaw.error);
        }

        if (!sessionKeyRaw.verified) {
            throw new Error('Container session key signature mismatch');
        }

        this.sessionKey = JSON.parse(sessionKeyRaw.plaintext);
    };

    /**!
     * ### sync(callback)
     * Retrieve history, decrypt it, and update
     * container object with new state
     *
     * Calls back without error if successful
     *
     * Calls back with error if unsuccessful
     *
     * @param {Function} callback
     */
    Container.prototype.sync = function(callback) {
        var that = this;
        this.getHistory(function(err, records) {
            if (err) {
                callback(err);
                return;
            }

            that.parseHistory(records, function(err, keys, versions, recordIndexAfter) {
                that.keys = keys;
                that.versions = versions;
                that.version = Math.max.apply(Math, Object.keys(versions));
                // versions.count is not defined:
                //that.recordCount = that.recordCount + versions.count;
                that.recordCount = Object.keys(versions).length;

                // TODO verify recordIndexAfter == recordCount?

                callback(err);
            });
        });
    };

    /**!
     * ### share(peer, callback)
     * Encrypt the container's sessionKey with peer's
     * public key, commit new addContainerSessionKey chunk,
     * and send a message to the peer informing them
     *
     * Calls back without error if successful
     *
     * Calls back with error if unsuccessful
     *
     * @param {Function} callback
     */
    Container.prototype.share = function(peer, callback) {
        if (!this.sessionKey) {
            return callback('Container must be initialized to share');
        }

        var containerNameHmac = this.getPublicName();
        if (containerNameHmac !== this.nameHmac) {
            callback("Only the creator of file can share it");
            return;
        }

        // encrypt sessionKey to peer's pubKey
        var sessionKeyCiphertext = peer.encryptAndSign(this.sessionKey);

        if (sessionKeyCiphertext.error) {
            return callback(sessionKeyCiphertext.error);
        }

        delete sessionKeyCiphertext.error;

        // create new addContainerSessionKeyShare chunk
        var that = this;

        var chunk = {
            toAccountId: peer.accountId,
            containerNameHmac: containerNameHmac,
            sessionKeyCiphertext: JSON.stringify(sessionKeyCiphertext),
        };

        var url = crypton.url() + '/container/share';
        superagent.post(url)
            //.withCredentials()
            .send(chunk)
            .use(crypton.bearer)
            .end(function(err, res) {
                if (!res.body || res.body.success !== true) {
                    callback(res.body.error);
                    return;
                }

                callback(null);
            });
    };

    Container.prototype.unshare = function(peer, callback) {
        if (!this.sessionKey) {
            return callback('Container must be initialized to share');
        }

        var containerNameHmac = this.getPublicName();
        if (containerNameHmac !== this.nameHmac) {
            callback("Only the creator of file can unshare it");
            return;
        }

        var that = this;
        var chunk = {
            toAccountId: peer.accountId,
            containerNameHmac: containerNameHmac,
        };

        var url = crypton.url() + '/container/unshare';
        superagent.post(url)
            //.withCredentials()
            .send(chunk)
            .use(crypton.bearer)
            .end(function(err, res) {
                if (!res.body || res.body.success !== true) {
                    callback(res.body.error);
                    return;
                }

                callback(null);
            });
    };

    /**!
     * ### watch(listener)
     * Attach a listener to the container
     * which is called if it is written to by a peer
     *
     * This is called after the container is synced
     *
     * @param {Function} callback
     */
    /*
    Container.prototype.watch = function (listener) {
      this._listener = listener;
    };
    */

    /**!
     * ### unwatch()
     * Remove an attached listener
     */
    Container.prototype.unwatch = function() {
        delete this._listener;
    };

})();

/* Crypton Client, Copyright 2013 SpiderOak, Inc.
 *
 * This file is part of Crypton Client.
 *
 * Crypton Client is free software: you can redistribute it and/or modify it
 * under the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Crypton Client is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the Affero GNU General Public
 * License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * along with Crypton Client.  If not, see <http://www.gnu.org/licenses/>.
 */

(function() {

    'use strict';

    /**!
     * # Peer(options)
     *
     * ````
     * var options = {
     *   username: 'friend' // required
     * };
     *
     * var peer = new crypton.Peer(options);
     * ````
     *
     * @param {Object} options
     */
    var Peer = crypton.Peer = function(options) {
        options = options || {};

        this.accountId = options.id;
        this.session = options.session;
        this.username = options.username;
        this.pubKey = options.pubKey;
        this.signKeyPub = options.signKeyPub;
    };

    /**!
     * ### fetch(callback)
     * Retrieve peer data from server, applying it to parent object
     *
     * Calls back with peer data and without error if successful
     *
     * Calls back with error if unsuccessful
     *
     * @param {Function} callback
     */
    Peer.prototype.fetch = function(callback) {
        if (!this.username) {
            callback('Must supply peer username');
            return;
        }

        if (!this.session) {
            callback('Must supply session to peer object');
            return;
        }

        var that = this;
        var url = crypton.url() + '/peer/' + this.username;
        superagent.get(url)
            .withCredentials()
            .use(crypton.bearer)
            .end(function(err, res) {
                if (!res.body || res.body.success !== true) {
                    callback(res.body.error);
                    return;
                }

                var peer = res.body.peer;
                that.accountId = peer.accountId;
                that.username = peer.username;
                that.pubKey = sjcl.ecc.deserialize(JSON.parse(peer.pubKey));
                that.signKeyPub = sjcl.ecc.deserialize(JSON.parse(peer.signKeyPub));

                // calculate fingerprint for public key
                that.fingerprint = crypton.fingerprint(that.pubKey, that.signKeyPub);

                callback(null, that);
            });
    };

    /**!
     * ### encrypt(payload)
     * Encrypt `message` with peer's public key
     *
     * @param {Object} payload
     * @return {Object} ciphertext
     */
    Peer.prototype.encrypt = function(payload) {
        if (!this.trusted) {
            return {
                error: 'Peer is untrusted'
            }
        }

        // should this be async to callback with an error if there is no pubkey?
        var ciphertext = sjcl.encrypt(this.pubKey, JSON.stringify(payload), crypton.cipherOptions);
        return ciphertext;
    };

    /**!
     * ### encryptAndSign(payload)
     * Encrypt `message` with peer's public key, sign the message with own signing key
     *
     * @param {Object} payload
     * @return {Object}
     */
    Peer.prototype.encryptAndSign = function(payload) {
        if (!this.trusted) {
            return {
                error: 'Peer is untrusted'
            }
        }

        try {
            var ciphertext = sjcl.encrypt(this.pubKey, JSON.stringify(payload), crypton.cipherOptions);
            // hash the ciphertext and sign the hash:
            var hash = sjcl.hash.sha256.hash(ciphertext);
            var signature = this.session.account.signKeyPrivate.sign(hash, crypton.paranoia);
            return {
                ciphertext: JSON.parse(ciphertext),
                signature: signature,
                error: null
            };
        } catch (ex) {
            logger.log("error", ex);
            logger.log("error", ex.stack);
            var err = "Error: Could not complete encryptAndSign: " + ex;
            return {
                ciphertext: null,
                signature: null,
                error: err
            };
        }
    };

    /**!
     * ### sendMessage(headers, payload, callback)
     * Encrypt `headers` and `payload` and send them to peer in one logical `message`
     *
     * Calls back with message id and without error if successful
     *
     * Calls back with error if unsuccessful
     *
     * @param {Object} headers
     * @param {Object} payload
     */
    Peer.prototype.sendMessage = function(headers, payload, callback) {
        if (!this.session) {
            callback('Must supply session to peer object');
            return;
        }

        var message = new crypton.Message(this.session);
        message.headers = headers;
        message.payload = payload;
        message.fromAccount = this.session.accountId;
        message.toAccount = this.accountId;
        message.encrypt(this);
        message.send(callback);
    };

    /**!
     * ### trust(callback)
     * Add peer's fingerprint to internal trusted peers Item
     *
     * Calls back without error if successful
     *
     * Calls back with error if unsuccessful
     *
     * @param {Function} callback
     */
    Peer.prototype.trust = function(callback) {
        var that = this;
        var container = e2ee.session.peersContainer
        container.get('peers', function(err, peers) {
            if (err) {
                logger.log("error", "Peers from peersContainer could not be retrieved:", err);
                callback(err)
            } else {
                peers[that.username] = {
                    trustedAt: +new Date(),
                    fingerprint: that.fingerprint
                };
                container.save(function(err) {
                    if (err) {
                        return callback(err);
                    }
                    that.trusted = true;
                    callback(null);
                });
            }
        })
    };

    Peer.prototype.untrust = function(callback) {
        var that = this;
        var container = e2ee.session.peersContainer
        container.get('peers', function(err, peers) {
            if (err) {
                logger.log("error", "Peers from peersContainer could not be retrieved:", err);
                callback(err)
            } else {
                delete peers[that.username]
                container.save(function(err) {
                    if (err) {
                        return callback(err);
                    }
                    that.trusted = true;
                    callback(null);
                });
            }
        })
    };

})();
/* Crypton Client, Copyright 2013 SpiderOak, Inc.
 *
 * This file is part of Crypton Client.
 *
 * Crypton Client is free software: you can redistribute it and/or modify it
 * under the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Crypton Client is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the Affero GNU General Public
 * License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * along with Crypton Client.  If not, see <http://www.gnu.org/licenses/>.
 */

(function() {

    'use strict';

    var Message = crypton.Message = function Message(session, raw) {
        this.session = session;
        this.headers = {};
        this.payload = {};

        raw = raw || {};
        for (var i in raw) {
            this[i] = raw[i];
        }
    };

    Message.prototype.encrypt = function(peer, callback) {
        var headersCiphertext = peer.encryptAndSign(this.headers);
        var payloadCiphertext = peer.encryptAndSign(this.payload);

        if (headersCiphertext.error || payloadCiphertext.error) {
            callback('Error encrypting headers or payload in Message.encrypt()');
            return;
        }

        this.encrypted = {
            headersCiphertext: JSON.stringify(headersCiphertext),
            payloadCiphertext: JSON.stringify(payloadCiphertext),
            fromUsername: this.session.account.username,
            toAccountId: peer.accountId
        };

        callback && callback(null);
    };

    Message.prototype.decrypt = function(callback) {
        var that = this;
        var headersCiphertext = JSON.parse(this.headersCiphertext);
        var payloadCiphertext = JSON.parse(this.payloadCiphertext);

        this.session.getPeer(this.fromUsername, function(err, peer) {
            if (err) {
                callback(err);
                return;
            }

            var headers = that.session.account.verifyAndDecrypt(headersCiphertext, peer);
            var payload = that.session.account.verifyAndDecrypt(payloadCiphertext, peer);
            if (!headers.verified || !payload.verified) {
                callback('Cannot verify headers or payload ciphertext in Message.decrypt()');
                return;
            } else if (headers.error || payload.error) {
                callback('Cannot decrypt headers or payload in Message.decrypt');
                return;
            }

            that.headers = JSON.parse(headers.plaintext);
            that.payload = JSON.parse(payload.plaintext);
            that.created = new Date(that.createdAt);

            callback(null, that);
        });
    };

    Message.prototype.send = function(callback) {
        if (!this.encrypted) {
            return callback('You must encrypt the message to a peer before sending!');
        }

        var url = crypton.url() + '/peer';
        superagent.post(url)
            .send(this.encrypted)
            .use(crypton.bearer)
            //.withCredentials()
            .end(function(err, res) {
                if (!res.body || res.body.success !== true) {
                    callback(res.body.error);
                    return;
                }

                callback(null, res.body.messageId);
            });
    };

})();
/* Crypton Client, Copyright 2013 SpiderOak, Inc.
 *
 * This file is part of Crypton Client.
 *
 * Crypton Client is free software: you can redistribute it and/or modify it
 * under the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Crypton Client is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the Affero GNU General Public
 * License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * along with Crypton Client.  If not, see <http://www.gnu.org/licenses/>.
 */

(function() {

    'use strict';

    var Diff = crypton.diff = {};

    /**!
     * ### create(old, current)
     * Generate an object representing the difference between two inputs
     *
     * @param {Object} old
     * @param {Object} current
     * @return {Object} delta
     */
    Diff.create = function(old, current) {
	// Downloading file from Swift is never chunked in the same way and 
	// there is no point in diffing two versions as it will be always
	// slower than diffing with {}. Furthermore, diff introduces some errors
	// and will be disabled by calling: old = {}.
	old = {}
        var delta = jsondiffpatch.diff(old, current);
        return delta;
    };

    /**!
     * ### apply(delta, old)
     * Apply `delta` to `old` object to build `current` object
     *
     * @param {Object} delta
     * @param {Object} old
     * @return {Object} current
     */
    // TODO should we switch the order of these arguments?
    Diff.apply = function(delta, old) {
        var current = JSON.parse(JSON.stringify(old)); // don't use $.extend(true, {}, old) here
        jsondiffpatch.patch(current, delta);
        return current;
    };

})();

/* Crypton Client, Copyright 2013 SpiderOak, Inc.
 *
 * This file is part of Crypton Client.
 *
 * Crypton Client is free software: you can redistribute it and/or modify it
 * under the terms of the Affero GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Crypton Client is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the Affero GNU General Public
 * License for more details.
 *
 * You should have received a copy of the Affero GNU General Public License
 * along with Crypton Client.  If not, see <http://www.gnu.org/licenses/>.
 */

(function() {

    'use strict';

    /*
     * if the browser supports web workers,
     * we "isomerize" crypton.work to transparently
     * put its methods in a worker and replace them
     * with a bridge API to said worker
     */
    /*
    !self.worker && window.addEventListener('load', function() {
        return;
        var scriptEls = document.getElementsByTagName('script');
        var path;

        for (var i in scriptEls) {
            if (scriptEls[i].src && ~scriptEls[i].src.indexOf('crypton.js')) {
                path = scriptEls[i].src;
            }
        }

        isomerize(crypton.work, path)
    }, false);
    */

    var work = crypton.work = {};

    /**!
     * ### unravelAccount(account, callback)
     * Decrypt account keys, and pass them back
     * in a serialized form for reconstruction
     *
     * Calls back with key object and without error if successful
     *
     * Calls back with error if unsuccessful
     *
     * @param {Object} account
     * @param {Function} callback
     */
    work.unravelAccount = function(account, callback) {
        var ret = {};

        var numRounds = crypton.MIN_PBKDF2_ROUNDS;
        // regenerate keypair key from password
        var keypairKey = sjcl.misc.pbkdf2(account.passphrase, account.keypairSalt, numRounds);
        var keypairMacKey = sjcl.misc.pbkdf2(account.passphrase, account.keypairMacSalt, numRounds);
        var signKeyPrivateMacKey = sjcl.misc.pbkdf2(account.passphrase, account.signKeyPrivateMacSalt, numRounds);

        var macOk = false;

        // decrypt secret key
        try {
            //var ciphertextString = JSON.stringify(account.keypairCiphertext);
            var ciphertextString = account.keypairCiphertext;
            macOk = crypton.hmacAndCompare(keypairMacKey, ciphertextString, account.keypairMac);
            ret.secret = JSON.parse(sjcl.decrypt(keypairKey, ciphertextString, crypton.cipherOptions));
        } catch (e) {}

        if (!macOk || !ret.secret) {
            // TODO could be decryption or parse error - should we specify?
            return callback('Could not parse secret key');
        }

        macOk = false;

        // decrypt signing key
        try {
            //var ciphertextString = JSON.stringify(account.signKeyPrivateCiphertext);
            var ciphertextString = account.signKeyPrivateCiphertext;
            macOk = crypton.hmacAndCompare(signKeyPrivateMacKey, ciphertextString, account.signKeyPrivateMac);
            ret.signKeySecret = JSON.parse(sjcl.decrypt(keypairKey, ciphertextString, crypton.cipherOptions));
        } catch (e) {}

        if (!macOk || !ret.signKeySecret) {
            return callback('Could not parse signKeySecret');
        }

        var secretKey = sjcl.ecc.deserialize(ret.secret);

        account.secretKey = secretKey;

        var session = {};
        session.account = account;
        session.account.signKeyPrivate = ret.signKeySecret;

        var selfPeer = new crypton.Peer({
            session: session,
            pubKey: account.pubKey,
            signKeyPub: account.signKeyPub
        });
        selfPeer.trusted = true;

        var selfAccount = new crypton.Account();
        selfAccount.secretKey = secretKey;

        // decrypt hmac keys
        var containerNameHmacKey;
        try {
            containerNameHmacKey = selfAccount.verifyAndDecrypt(account.containerNameHmacKeyCiphertext, selfPeer);
            ret.containerNameHmacKey = JSON.parse(containerNameHmacKey.plaintext);
        } catch (e) {}

        if (!containerNameHmacKey.verified) {
            // TODO could be decryption or parse error - should we specify?
            return callback('Could not parse containerNameHmacKey');
        }

        var hmacKey;
        try {
            hmacKey = selfAccount.verifyAndDecrypt(account.hmacKeyCiphertext, selfPeer);
            ret.hmacKey = JSON.parse(hmacKey.plaintext);
        } catch (e) {}

        if (!hmacKey.verified) {
            // TODO could be decryption or parse error - should we specify?
            return callback('Could not parse hmacKey');
        }

        callback(null, ret);
    };

    /**!
     * ### decryptRecord(options, callback)
     * Decrypt a single record after checking its signature
     *
     * Calls back with decrypted record and without error if successful
     *
     * Calls back with error if unsuccessful
     *
     * @param {Object} options
     * @param {Function} callback
     */
    work.decryptRecord = function(options, callback) {
        var sessionKey = options.sessionKey;
        var creationTime = options.creationTime;
        var expectedRecordIndex = options.expectedRecordIndex;
        var peerSignKeyPubSerialized = options.peerSignKeyPubSerialized;

        if (!sessionKey ||
            !creationTime ||
            !expectedRecordIndex ||
            !peerSignKeyPubSerialized
        ) {
            return callback('Must supply all options to work.decryptRecord');
        }

        var record;
        try {
            record = JSON.parse(options.record);
        } catch (e) {}

        if (!record) {
            return callback('Could not parse record');
        }

        // reconstruct the peer's public signing key
        // the key itself typically has circular references which
        // we can't pass around with JSON to/from a worker
        var peerSignKeyPub = sjcl.ecc.deserialize(peerSignKeyPubSerialized);

        var verified = false;

        var bytes = [];
        var str = JSON.stringify(record.ciphertext);
        for (var i = 0; i < str.length; ++i) {
            bytes.push(str.charCodeAt(i));
        }
        var b = new BLAKE2s(32);
        b.update(bytes);
        var payloadCiphertextHash = b.digest("hex");

        //var payloadCiphertextHash = sjcl.hash.sha256.hash(JSON.stringify(record.ciphertext));

        try {
            verified = peerSignKeyPub.verify(payloadCiphertextHash, record.signature);
        } catch (e) {
            logger.log("error", e);
        }

        if (!verified) {
            return callback('Record signature does not match expected signature');
        }

        var payload = {};
        //var payload;
        try {
            var dec = sjcl.decrypt(sessionKey, record.ciphertext, crypton.cipherOptions);
            if (typeof(dec) === "string") {
                payload = JSON.parse(dec);
            } else { // ArrayBuffer (when encrypted with CCM and as ArrayBuffer)
                payload.delta = {};
                payload.delta.chunks = [];
                var o = {};

                var separationOffset = 10;
                var metadataStart;
                var newArray = new Uint8Array(dec);
                for (var j = newArray.length; j >= 0; j--) {
                    if (newArray[j] == 0) {
                        var separator = newArray.slice(j - separationOffset + 1, j + 1)
                        if (separator.join() === new Uint8Array(separationOffset).join()) {
                            metadataStart = j + 1;
                            break;
                        }
                    }
                }
                payload.delta.metadata = [];
                var metaString = String.fromCharCode.apply(null, newArray.slice(metadataStart));
                var metaObj = JSON.parse(metaString);
                payload.delta.metadata.push(metaObj);

                var chunksDec = dec.slice(0, metadataStart - separationOffset);
                var chunksNum = parseInt(chunksDec.byteLength / e2ee.crypto.chunkSize) + 1;
                var startChunk = 0;
                for (var i = 0; i < chunksNum; i++) {
                    var min = Math.min(startChunk + e2ee.crypto.chunkSize, chunksDec.byteLength);
                    var chunk = new Uint8Array(chunksDec.slice(startChunk, min));
                    o[startChunk] = chunk;
                    startChunk += e2ee.crypto.chunkSize;
                }
                payload.delta.chunks.push(o);
            }
        } catch (e) {
            logger.log("error", e)
        }

        if (!payload) {
            return callback('Could not parse record payload');
        }

        if (payload.recordIndex !== expectedRecordIndex) {
            // TODO revisit
            // XXX ecto 3/4/14 I ran into a problem with this quite a while
            // ago where recordIndexes would never match even if they obviously
            // should. It smelled like an off-by-one or state error.
            // Now that record decryption is abstracted outside container instances,
            // we will have to do it in a different way anyway
            // (there was formerly a this.recordIndex++ here)

            // return callback('Record index mismatch');
        }

        callback(null, {
            time: +new Date(creationTime),
            delta: payload.delta
        });
    };

})();
