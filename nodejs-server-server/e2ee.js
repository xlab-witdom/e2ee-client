var fs = require('fs');
var nconf = require("nconf");
var pkgcloud = require('pkgcloud');
var chunkingStreams = require('chunking-streams');
var SizeChunker = chunkingStreams.SizeChunker;
var request = require('request');

var e2ee

var loggers = require("./log");
var logger = loggers.logger;
var perfLogger = loggers.perfLogger;

var tEncrypt
var tDecrypt

(function() {
    'use strict';

    e2ee = {}
    e2ee.settings = {}

    exports.session = {
        isReady: false,
        cryptonSession: null,
        indexContainer: null,
        keys: {},
        keyPairReady: false,
        currentFile: {
            fileObject: null,
            fileName: '',
            encryptedChunks: [],
            decryptedChunks: []
        }
    }

    e2ee.util = {}

    e2ee.util.resetCurrentFile = function() {
        delete exports.session.currentFile
        exports.session.currentFile = {
            fileObject: null,
            fileName: '',
            encryptedChunks: [],
            decryptedChunks: [],
            streamEncryptor: null,
            streamDecryptor: null
        }
    }

    exports.crypto = {}

    // Chunk size (in bytes)
    // Warning: Must not be less than 256 bytes
    exports.crypto.chunkSize = 1024 * 1024 * 1

    exports.crypto.addChunkToContainer = function(chunk_id, chunk, fileContainer) {
        fileContainer.get('chunks', function(err, chunks) {
            if (err) {
                logger.log("error", err, '- value for chunks key of the file (that is being encrypted) container cannot be retrieved');
            } else {
                chunks[chunk_id] = chunk
            }
        })
    }

    exports.crypto.deleteFile = function(fileName, callback) {
        exports.session.cryptonSession.deleteContainer(fileName, function(err, container) {
            if (err) {
                logger.log("error", "Error deleting container:", err);
            } else {
                callback()
            }
        })
    }

    exports.crypto.notifyAboutUnshared = function(file, callback) {
        // when file that was shared is deleted, the unshare notification should be sent
        exports.session.cryptonSession.getPeer(file.user, function callback1(err, peer) {
            if (err) {
                if (window.console && window.console.log) {
                    logger.log("error", "Error getting peer:", err);
                }
            }
            var headers = {}
            var payload = {
                operation: 'unshare',
                fileName: file.fileName
            }
            peer.sendMessage(headers, payload, function(err, messageId) {
                if (err) {
                    logger.log("error", "Error sending message:", err);
                }
                callback()
            })
        })
    }

    exports.crypto.getContainerByHmac = function(fileHmacName, peerName, fileName, callback1) {
        exports.session.cryptonSession.getPeer(peerName, function callback(err, peer) {
            if (err) {
                logger.log("error", err);
                return
            }
            exports.session.cryptonSession.loadWithHmac(fileHmacName, peer, function(err, fileContainer) {
                if (err) {
                    logger.log("error", err);
                } else {
                    fileContainer.name = fileName // when loading with hmac, file name is not set
                    callback1(fileContainer)
                }
            })
        })
    }

    exports.crypto.downloadFileByHmac = function(hmac, peerName, fileName) {
        exports.crypto.getContainerByHmac(hmac, peerName, fileName, function(fileContainer) {
            fileContainer.get('chunks', function(err, chunks) {
                if (err) {
                    logger.log("error", "File not available:", hmac, "(", err, ")");
                } else {
                    // this is to be replace with SWIFT object:
                    var filePath = 'README.md';
                    var f = fs.createWriteStream(filePath);
                    Object.keys(chunks).forEach(function(key) { // keys are data positions
                        var a = chunks[key]
                        // convert object to array of values:
                        var arr = Object.keys(a).map(function(key) {
                            return a[key]
                        })
                        //var v = String.fromCharCode.apply(null, arr) // if you want to see the plaintext
                        var b = new Buffer(arr)
                        f.write(b);
                    });
                    f.end();
                }
            })
        })
    }

    exports.crypto.getContainer = function(name, callbackUrl, callback) {
        var processId = callbackUrl.split("/")[6]

        exports.session.cryptonSession.load(name, function(err, fileContainer) {
            if (err) {
                var success = false;
                var msg = "Failed to retrieve Crypton container '" + name + "' (" + err + ")";
                logger.log('error', msg);
                e2ee.callPO(callbackUrl, success, msg, function(){
                    perfLogger.log("info", "[", processId, "] Elapsed [", new Date() - tDecrypt, "]. Process finished with status [failure]");
                })
            } else {
                logger.log('info', "Container", name, "successfully retrieved");
                callback(fileContainer)
            }
        })
    }

    exports.crypto.downloadFile = function(fileName, swiftContainerName, swiftObjectName, host, callbackUrl) {
        var processId = callbackUrl.split("/")[6]
        
        console.log();
        perfLogger.log("info", "[", processId , "] Received [decrypt] from [", host, "]");
        tDecrypt = new Date();

        exports.session.indexContainer.get('fileNames', function(err, fileNames) {
            if (err) {
                logger.log("error", "file names from indexContainer could not be retrieved:", err);
                return
            }
            var filesList = fileNames['notMineFiles'] //# <--?
            for (var i = 0; i < filesList.length; i++) {
                var file = filesList[i]
                if (file.fileName === fileName) {
                    exports.crypto.downloadFileByHmac(file.hmac, file.peer, file.fileName)
                    logger.log("info", "File to download:", fileName);
                    return
                }
            }

            var tGetContainer = new Date();
            exports.crypto.getContainer(fileName, callbackUrl, function(fileContainer) {
                fileContainer.get('chunks', function(err, chunks) {
                    perfLogger.log("info", "[", processId, "] Elapsed [", new Date() - tGetContainer, "] for [getCryptonContainer/E2EE-server]");
                    if (err) {
                        var msg = 'File not available: ' + fileName
                        logger.log("info", msg);
                        var success = false
                        e2ee.callPO(callbackUrl, success, msg, function(){
                            perfLogger.log("info", "[", processId, "] Elapsed [", new Date() - tDecrypt, "]. Process finished with status [failure]");
                        })
                    } else {
                        nconf.file({
                            file: '../config.json'
                        });
                        var saveTo = nconf.get('saveDecryptedFileTo');

                        var uploadToSwift = false;
                        if (saveTo == "swift") {
                            uploadToSwift = true;
                        }

                        if (uploadToSwift) {
                            var tUploadToSwift = new Date();
                            logger.log("info", "Uploading object", swiftObjectName, "to Swift container", swiftContainerName);

                            var swiftAddress = nconf.get('swift-address');
                            var swiftPort = nconf.get('swift-port');
                            var swiftUsername = nconf.get('swift-username'); // todo
                            var swiftPassword = nconf.get('swift-password'); // todo

                            var client = pkgcloud.storage.createClient({
                                provider: 'openstack',
                                username: swiftUsername,
                                password: swiftPassword,
                                authUrl: swiftAddress + ":" + swiftPort,
                                version: 1
                            });

                            var writeStream = client.upload({
                                container: swiftContainerName,
                                remote: swiftObjectName
                            });

                            writeStream.on('error', function(err) {
                                logger.log("error", err)
                                var success = false
                                e2ee.callPO(callbackUrl, success, err, function() {
                                    perfLogger.log("info", "[", processId, "] Elapsed [", new Date() - tUploadToSwift, "] for [uploadFile/witdom-object-storage]");
                                    perfLogger.log("info", "[", processId, "] Elapsed [", new Date() - tEncrypt, "]. Process finished with status [failure]");
                                })  
                            })

                            Object.keys(chunks).forEach(function(key) { // keys are data positions
                                var a = chunks[key]
                                // convert object to array of values:
                                var arr = Object.keys(a).map(function(key) {
                                    return a[key]
                                })
                                //var v = String.fromCharCode.apply(null, arr) // if you want to see the plaintext
                                var b = new Buffer(arr)
                                writeStream.write(b);
                                logger.log("debug", "uploadFile - Writing chunk")
                            });
                            writeStream.end();
                            logger.log("debug", "uploadFile - done!")

                            writeStream.on("success", function() {
                            perfLogger.log("info", "[", processId, "] Elapsed [", new Date() - tUploadToSwift, "] for [uploadFile/witdom-object-storage]");
                            var msg = "File was decrypted and uploaded to Swift."
                            var success = true
                            e2ee.callPO(callbackUrl, success, msg, function(){
                                perfLogger.log("info", "[", processId, "] Elapsed [", new Date() - tDecrypt, "]. Process finished with status [success]");
                            })
                        });             
                    } else { // to local file
                            var filePath = fileName;
                            logger.log("info", "Writing to file:", filePath);
                            var f = fs.createWriteStream(filePath);
                            Object.keys(chunks).forEach(function(key) { // keys are data positions
                                var a = chunks[key]
                                // convert object to array of values:
                                var arr = Object.keys(a).map(function(key) {
                                    return a[key]
                                })
                                //var v = String.fromCharCode.apply(null, arr) // if you want to see the plaintext
                                var b = new Buffer(arr)
                                f.write(b);
                            });
                            f.end();
                        }
                    }
                })
            })
        })
    }

    exports.crypto.encryptLocalFile = function(file) {
        exports.session.currentFile.fileName = file.name

        exports.session.cryptonSession.load(file.name, function(err, fileContainer) {
            logger.log("info", "Trying to load Crypton container");
            if (err) { // the file is not stored on Crypton yet
                logger.log("info", err);
                exports.session.cryptonSession.create(file.name, function(err, fileContainer) {
                    logger.log("info"," Creating Crypton container for file:", file.name);
                    if (err) {
                        logger.log("error", "Crypton container for the file that is being encrypted could not be created:", err);
                    } else {
                        logger.log("info", "Creation of Crypton container successful. Chunks key will be added to fileContainer");
                        fileContainer.add('chunks', function() {
                            var alreadyExists = false
                            exports.crypto.addNextChunkToContainer(file, fileContainer, 0, alreadyExists)
                        })
                    }
                })
            } else { // the file will be overwritten on Crypton
                logger.log("info", "Creation of Crypton container successful. Chunks key will be added to fileContainer");
                var alreadyExists = true
                exports.crypto.addNextChunkToContainer(file, fileContainer, 0, alreadyExists)
            }
        })
    }

    exports.crypto.addNextChunkToContainer = function(file, fileContainer, dataPosition, alreadyExists) {
        logger.log("Adding next chunk");
        e2ee.file.read(
            file,
            dataPosition,
            dataPosition + exports.crypto.chunkSize,
            function(chunk) {
                var isLast = false
                if (dataPosition >= (file.size - exports.crypto.chunkSize)) {
                    isLast = true
                }
                var encryptedChunk

                exports.crypto.addChunkToContainer(dataPosition, chunk, fileContainer) // todo: this contains async call, but is used as sync
                if (isLast) {
                    fileContainer.add('metadata', function() {
                        fileContainer.get('metadata', function(err, meta) {
                            if (err) {
                                logger.log("error", "Value for metadata key of the file (that is being encrypted) container cannot be retrieved:", err);
                            } else {
                                var metadata = "added " + new Date().toJSON().slice(0, 10)
                                meta["date"] = metadata
                                fileContainer.save(function(err) {
                                    if (err) {
                                        logger.log("error", err);
                                    } else {
                                        if (!alreadyExists) {
                                            exports.session.indexContainer.get('fileNames', function(err, fileNames) {
                                                fileNames['listOfFiles'].push(file.name)
                                                exports.session.indexContainer.save(function(err) {
                                                    if (err) {
                                                        logger.log("error", "Name of the file that is being encrypted could not be stored in indexContainer", err);
                                                    }
                                                })
                                            })
                                        }
                                        logger.log("info", "Encryption was successful");
                                    }
                                })
                            }
                        })
                    })
                } else {
                    dataPosition += exports.crypto.chunkSize
                    return exports.crypto.addNextChunkToContainer(
                        file,
                        fileContainer,
                        dataPosition,
                        alreadyExists
                    )
                }
            }
        )
    }

    exports.crypto.encryptSwiftFile = function(swiftContainerName, fileName, callbackUrl) {
        var processId = callbackUrl.split("/")[6]

        exports.session.currentFile.fileName = fileName
        var tLoadCryptonContainer = new Date();
        exports.session.cryptonSession.load(fileName, function(err, fileContainer) {
            logger.log("info", "Trying to load Crypton container for file:", fileName);
            if (err) { // the file is not stored on Crypton yet
                logger.log("info", "The file is not stored on Crypton, will create new container:", err);
                perfLogger.log("info", "[", processId, "] Elapsed [", new Date() - tLoadCryptonContainer, "] for [loadCryptonContainer/E2EE-server]");
                var tCreateCryptonContainer = new Date();
                exports.session.cryptonSession.create(fileName, function(err, fileContainer) {
                    logger.log("info", "Creating  Crypton container for file", fileName);
                    if (err) {
                        logger.log("error", "Failed to create Crypton container for the file that is being encrypted:", err);
                        perfLogger.log("info", "[", processId, "] Elapsed [", new Date() - tCreateCryptonContainer, "] for [createCryptonContainer/E2EE-server]");
                        var success = false
                        e2ee.callPO(callbackUrl, success, err, function() {
                            perfLogger.log("info", "[", processId, "] Elapsed [", new Date() - tEncrypt, "]. Process finished with status [failure]");
                        })
                    } else {
                        logger.log("info", "Successfuly created new Crypton container. Chunks key will be added to fileContainer.");
                        perfLogger.log("info", "[", processId, "] Elapsed [", new Date() - tCreateCryptonContainer, "] for [createCryptonContainer/E2EE-server]");
                        fileContainer.add('chunks', function() {
                            var alreadyExists = false
                            exports.crypto.addToContainer(swiftContainerName, fileName, fileContainer, alreadyExists, callbackUrl)
                        })
                    }
                })
            } else { // the file will be overwritten on Crypton
                logger.log("info", "Container for the file already exists (no need to create a new container)");
                perfLogger.log("info", "[", processId, "] Elapsed [", new Date() - tLoadCryptonContainer, "] for [createCryptonContainer/E2EE-server]");
                var alreadyExists = true
                exports.crypto.addToContainer(swiftContainerName, fileName, fileContainer, alreadyExists, callbackUrl)
            }
        })
    }

    // Encrypts a file and and stores it to Swift
    exports.crypto.addToContainer = function(swiftContainerName, fileName, fileContainer, alreadyExists, callbackUrl) {
        var processId = callbackUrl.split("/")[6]
        var tSwiftDownloadFile = new Date();

        logger.log("debug", "In: exports.crypto.addToContainer", "CONTAINER:", swiftContainerName, "FILE:", fileName, "ALREADY EXISTS:", alreadyExists);
        nconf.file({
            file: '../config.json'
        });
        var swiftAddress = nconf.get('swift-address');
        var swiftPort = nconf.get('swift-port');
        var swiftUsername = nconf.get('swift-username'); // todo
        var swiftPassword = nconf.get('swift-password'); // todo

        var client = pkgcloud.storage.createClient({
            provider: 'openstack',
            username: swiftUsername,
            password: swiftPassword,
            authUrl: swiftAddress + ":" + swiftPort,
            version: 1
        });


        var chunker = new SizeChunker({
            chunkSize: exports.crypto.chunkSize
        })

        var stream = client.download({
            container: swiftContainerName,
            remote: fileName
        });
        stream.on('error', function(err) {
            logger.log("error", err)
            var success = false
            e2ee.callPO(callbackUrl, success, err, function() {
                perfLogger.log("info", "[", processId, "] Elapsed [", new Date() - tSwiftDownloadFile, "] for [downloadRemoteFile/witdom-object-storage]");
                perfLogger.log("info", "[", processId, "] Elapsed [", new Date() - tEncrypt, "]. Process finished with status [failure]");
            })  
        })

        chunker.on('chunkStart', function(id, done) {
            done();
        });

        chunker.on('chunkEnd', function(id, done) {
            done();
        });

        var dataPosition = 0;

        chunker.on('data', function(chunk) {
            exports.crypto.addChunkToContainer(dataPosition, chunk.data, fileContainer) // todo: this contains async call, but is used as sync
            dataPosition += chunk.data.length
        });

        stream.pipe(chunker)
        stream.on('finish', function() {
            perfLogger.log("info", "[", processId, "] Elapsed [", new Date() - tSwiftDownloadFile, "] for [downloadRemoteFile/witdom-object-storage]");
            fileContainer.add('metadata', function() {
                fileContainer.get('metadata', function(err, meta) {
                    if (err) {
                        logger.log("error", "Calue for metadata key of the file (that is being encrypted) container cannot be retrieved:", err);
                        var success = false
                        e2ee.callPO(callbackUrl, success, err, function() {
                            perfLogger.log("info", "[", processId, "] Elapsed [", new Date() - tEncrypt, "]. Process finished with status [failure]");
                        })
                    } else {
                        var metadata = "added " + new Date().toJSON().slice(0, 10)
                        meta["date"] = metadata
                        var tEncryptData = new Date();
                        fileContainer.save(function(err) {
                            if (err) { // File has not changed
                                // In case file has not changes, report success, not failure
                                logger.log("info", err, ", reporting success");
                                var success = true;//false
                                e2ee.callPO(callbackUrl, success, err, function() {
                                    perfLogger.log("info", "[", processId, "] Elapsed [", new Date() - tEncrypt, "]. Process finished with status [success]");
                                })
                            } else {
                                if (!alreadyExists) {
                                    exports.session.indexContainer.get('fileNames', function(err, fileNames) {
                                        fileNames['listOfFiles'].push(fileName)
                                        exports.session.indexContainer.save(function(err) {
                                            if (err) {
                                                logger.log("error", "Name of the file that is being encrypted could not be stored in indexContainer:", err);
                                            }
                                        })
                                    })
                                }
                                var msg = 'Encryption was successful.'
                                logger.log("info", msg);
                                perfLogger.log("info", "[", processId, "] Elapsed [", new Date() - tEncryptData, "] for [encryptData]");
                                var success = true
                                e2ee.callPO(callbackUrl, success, msg, function() {
                                    perfLogger.log("info", "[", processId, "] Elapsed [", new Date() - tEncrypt, "]. Process finished with status [success]");
                                })
                            }
                        })
                    }
                })
            })

        });
    }

    e2ee.file = {}

    e2ee.file.read = function(file, start, end, callback, errorCallback) {
        if (end > file.size) {
            end = file.size
        }
        var buffer = new Buffer(end - start)
        fs.read(file.fd, buffer, 0, end - start, start)
        return callback(buffer)

    }

    e2ee.callPO = function(callbackUrl, success, msg, f) {
        var processId = callbackUrl.split("/")[6]
        var tCallToPO = new Date();
        
        logger.log("info", "Contacting PO (performing callback) at:", callbackUrl);
        var status = success ? "success" : "failure";

        var options = {
            url: callbackUrl,
            headers: {
                'X-Auth-Token': 'todo'
            },
            json: {
                "status": status
            }
        };
        options['json']['results'] = {} 
        options['json']['results'][status] = msg


        function callback(error, response, body) {
            perfLogger.log("info", "[", processId, "] Elapsed [", new Date() - tCallToPO, "] for [sendCallback/PO]");
            if (!error) {
                logger.log("info", "Response from the PO:", response.statusCode, body);
            } else {
                logger.log("error", "Error ocurred during the PO callback:", error);
            }
            f();
        }

        logger.log("debug", "This is what we're sending to the PO:", JSON.stringify(options));
        request.post(options, callback);
    }

    exports.encrypt = function encrypt(containerName, objectName, host, callbackUrl) {
        var processId = callbackUrl.split("/")[6]
        
        console.log();
        perfLogger.log("info", "[", processId , "] Received [encrypt] from [", host, "]");
        tEncrypt = new Date();

        logger.log("info", "Loading file", objectName, "from Swift container", containerName, "for encryption");

        nconf.file({
            file: '../config.json'
        });
        var readFileFrom = nconf.get('readFileFrom');

        var takeFileFromSwift = false;
        if (readFileFrom == "swift") {
            takeFileFromSwift = true;
        }

        if (takeFileFromSwift) {
            e2ee.util.resetCurrentFile()
            //exports.session.currentFile.fileObject = file // todo
            exports.crypto.encryptSwiftFile(containerName, objectName, callbackUrl)
        } else {
            var filePath = objectName;
            var fs = require("fs");
            var stats = fs.statSync(filePath)
            var fileSizeInBytes = stats["size"]

            fs.open(filePath, 'r', (err, fd) => {
                if (err) {
                    logger.log("error", err);
                }
                var file = {
                    fd: fd,
                    name: objectName,
                    size: fileSizeInBytes
                }

                e2ee.util.resetCurrentFile()
                exports.session.currentFile.fileObject = file
                exports.crypto.encryptLocalFile(file)
            });
        }
    }
})()
