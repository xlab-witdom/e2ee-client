# E2EeClientApi.DecryptParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**callbackURL** | **String** | Callback url where to respond to a PO asynchronous call | [optional] 
**objectName** | **String** | Name which was used when encryption was called. | 
**location** | [**DataLocation**](DataLocation.md) |  | 


