# E2EeClientApi.E2eeLoginCredentials

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**passphrase** | **String** | Passphrase used to generate E2EE master key. | [optional] 


