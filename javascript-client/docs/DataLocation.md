# E2EeClientApi.DataLocation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**host** | **String** | Address of the object storage database | [optional] 
**port** | **String** | Port on which the object storage services are accessible | [optional] 
**containerName** | **String** | Object storage container name | [optional] 
**objectName** | **String** | Name of the object to be retrieved (or created/updated in the case of decryption) | [optional] 
**dbCredentials** | [**DbUserPassCredentials**](DbUserPassCredentials.md) |  | [optional] 


