# E2EeClientApi.DbUserPassCredentials

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **String** | Username | [optional] 
**password** | **String** | Password | [optional] 


