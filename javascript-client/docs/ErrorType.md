# E2EeClientApi.ErrorType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **Number** | Integer code of the error | [optional] 
**message** | **String** | Description of the error | [optional] 


