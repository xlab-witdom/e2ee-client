# E2EeClientApi.DefaultApi

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**decryptPost**](DefaultApi.md#decryptPost) | **POST** /decrypt | Decrypt an object in untrusted domain and store it in object storage database
[**encryptPost**](DefaultApi.md#encryptPost) | **POST** /encrypt | Encrypt an object from object storage database
[**openSessionPost**](DefaultApi.md#openSessionPost) | **POST** /openSession | Initialize session


<a name="decryptPost"></a>
# **decryptPost**
> decryptPost(xAuthToken, payload)

Decrypt an object in untrusted domain and store it in object storage database

It retrieves the object from the WITDOM untrusted domain, decrypts it and stores it into the object storage database.

### Example
```javascript
var E2EeClientApi = require('e2_ee_client_api');

var apiInstance = new E2EeClientApi.DefaultApi();

var xAuthToken = "xAuthToken_example"; // String | The authentication token to be verified

var payload = new E2EeClientApi.DecryptParams(); // DecryptParams | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.decryptPost(xAuthToken, payload, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xAuthToken** | **String**| The authentication token to be verified | 
 **payload** | [**DecryptParams**](DecryptParams.md)|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="encryptPost"></a>
# **encryptPost**
> encryptPost(xAuthToken, payload)

Encrypt an object from object storage database

It retrieves the object from the object storage database, encrypts it and stores it into the database in a WITDOM untrusted domain.

### Example
```javascript
var E2EeClientApi = require('e2_ee_client_api');

var apiInstance = new E2EeClientApi.DefaultApi();

var xAuthToken = "xAuthToken_example"; // String | The authentication token to be verified

var payload = new E2EeClientApi.EncryptParams(); // EncryptParams | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.encryptPost(xAuthToken, payload, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xAuthToken** | **String**| The authentication token to be verified | 
 **payload** | [**EncryptParams**](EncryptParams.md)|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="openSessionPost"></a>
# **openSessionPost**
> openSessionPost(xAuthToken, payload)

Initialize session

Initialize session - prepare keys which will be used for encryption.

### Example
```javascript
var E2EeClientApi = require('e2_ee_client_api');

var apiInstance = new E2EeClientApi.DefaultApi();

var xAuthToken = "xAuthToken_example"; // String | The authentication token to be verified

var payload = new E2EeClientApi.E2eeLoginCredentials(); // E2eeLoginCredentials | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.openSessionPost(xAuthToken, payload, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xAuthToken** | **String**| The authentication token to be verified | 
 **payload** | [**E2eeLoginCredentials**](E2eeLoginCredentials.md)|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

