# E2EeClientApi.EncryptParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**callbackURL** | **String** | Callback url where to respond to a PO asynchronous call | [optional] 
**location** | [**DataLocation**](DataLocation.md) |  | 


