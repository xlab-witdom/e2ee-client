/**
 * E2EE client API
 * Description of the API of WITDOM's trusted-domain End to End Encryption component.
 *
 * OpenAPI spec version: 0.1.0
 * Contact: miha.stopar@xlab.si
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/ErrorType', 'model/DecryptParams', 'model/EncryptParams', 'model/E2eeLoginCredentials'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/ErrorType'), require('../model/DecryptParams'), require('../model/EncryptParams'), require('../model/E2eeLoginCredentials'));
  } else {
    // Browser globals (root is window)
    if (!root.E2EeClientApi) {
      root.E2EeClientApi = {};
    }
    root.E2EeClientApi.DefaultApi = factory(root.E2EeClientApi.ApiClient, root.E2EeClientApi.ErrorType, root.E2EeClientApi.DecryptParams, root.E2EeClientApi.EncryptParams, root.E2EeClientApi.E2eeLoginCredentials);
  }
}(this, function(ApiClient, ErrorType, DecryptParams, EncryptParams, E2eeLoginCredentials) {
  'use strict';

  /**
   * Default service.
   * @module api/DefaultApi
   * @version 0.1.0
   */

  /**
   * Constructs a new DefaultApi. 
   * @alias module:api/DefaultApi
   * @class
   * @param {module:ApiClient} apiClient Optional API client implementation to use,
   * default to {@link module:ApiClient#instance} if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;


    /**
     * Callback function to receive the result of the decryptPost operation.
     * @callback module:api/DefaultApi~decryptPostCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Decrypt an object in untrusted domain and store it in object storage database
     * It retrieves the object from the WITDOM untrusted domain, decrypts it and stores it into the object storage database.
     * @param {String} xAuthToken The authentication token to be verified
     * @param {module:model/DecryptParams} payload 
     * @param {module:api/DefaultApi~decryptPostCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.decryptPost = function(xAuthToken, payload, callback) {
      var postBody = payload;

      // verify the required parameter 'xAuthToken' is set
      if (xAuthToken == undefined || xAuthToken == null) {
        throw new Error("Missing the required parameter 'xAuthToken' when calling decryptPost");
      }

      // verify the required parameter 'payload' is set
      if (payload == undefined || payload == null) {
        throw new Error("Missing the required parameter 'payload' when calling decryptPost");
      }


      var pathParams = {
      };
      var queryParams = {
      };
      var headerParams = {
        'X-Auth-Token': xAuthToken
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = [];
      var returnType = null;

      return this.apiClient.callApi(
        '/decrypt', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the encryptPost operation.
     * @callback module:api/DefaultApi~encryptPostCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Encrypt an object from object storage database
     * It retrieves the object from the object storage database, encrypts it and stores it into the database in a WITDOM untrusted domain.
     * @param {String} xAuthToken The authentication token to be verified
     * @param {module:model/EncryptParams} payload 
     * @param {module:api/DefaultApi~encryptPostCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.encryptPost = function(xAuthToken, payload, callback) {
      var postBody = payload;

      // verify the required parameter 'xAuthToken' is set
      if (xAuthToken == undefined || xAuthToken == null) {
        throw new Error("Missing the required parameter 'xAuthToken' when calling encryptPost");
      }

      // verify the required parameter 'payload' is set
      if (payload == undefined || payload == null) {
        throw new Error("Missing the required parameter 'payload' when calling encryptPost");
      }


      var pathParams = {
      };
      var queryParams = {
      };
      var headerParams = {
        'X-Auth-Token': xAuthToken
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = [];
      var returnType = null;

      return this.apiClient.callApi(
        '/encrypt', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the openSessionPost operation.
     * @callback module:api/DefaultApi~openSessionPostCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Initialize session
     * Initialize session - prepare keys which will be used for encryption.
     * @param {String} xAuthToken The authentication token to be verified
     * @param {module:model/E2eeLoginCredentials} payload 
     * @param {module:api/DefaultApi~openSessionPostCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.openSessionPost = function(xAuthToken, payload, callback) {
      var postBody = payload;

      // verify the required parameter 'xAuthToken' is set
      if (xAuthToken == undefined || xAuthToken == null) {
        throw new Error("Missing the required parameter 'xAuthToken' when calling openSessionPost");
      }

      // verify the required parameter 'payload' is set
      if (payload == undefined || payload == null) {
        throw new Error("Missing the required parameter 'payload' when calling openSessionPost");
      }


      var pathParams = {
      };
      var queryParams = {
      };
      var headerParams = {
        'X-Auth-Token': xAuthToken
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = [];
      var returnType = null;

      return this.apiClient.callApi(
        '/openSession', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }
  };

  return exports;
}));
