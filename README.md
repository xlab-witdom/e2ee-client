# End-to-End Encryption (E2EE) storage client

E2EE client encrypts files and sends them to [E2EE server](https://github.com/xlab-si/e2ee-server). For cryptography it uses [Crypton](https://github.com/SpiderOak/crypton). However, Crypton was [slightly modified](https://github.com/xlab-si/e2ee-client/wiki/Speed-up-Javascript-crypto) to enable faster encryption/decryption of files. 

In WITDOM, E2EE takes a file from Openstack Swift, encrypts it and uploads to E2EE server.
When a file from E2EE server is needed, E2EE clients downloads it, decrypts it and uploads it to Swift.

# Installation 

## Server code generation

If the API (defined in integration-tests/e2eeSwagger.json) changes, the code needs to be regenerated. 

Generate Node.js server code on [1] using e2eeSwagger.json in integration-tests. Move the generated nodejs-server-server folder into e2ee-client, replace stubs in controllers with the actual code, like:

```
'use strict';

var crypto = require('../e2eecrypto');
var e2ee = require('../e2ee');

exports.decryptPOST = function(args, res, next) {
  var objectName = args.payload.value.objectName
  var destContainerName = args.payload.value.location.containerName
  var destObjectName = args.payload.value.location.objectName
  
  var callbackUrl = args.payload.value.callbackURL
  e2ee.crypto.downloadFile(objectName, destContainerName, destObjectName, callbackUrl);
  res.end();
}

exports.encryptPOST = function(args, res, next) {
  var containerName = args.payload.value.location.containerName
  var objectName = args.payload.value.location.objectName
  var callbackUrl = args.payload.value.callbackURL
  e2ee.encrypt(containerName, objectName, callbackUrl);
  res.end();
}

exports.openSessionPOST = function(args, res, next) {
  var username = "bla"
  var passphrase = args.payload.value.passphrase
  crypto.openSession(username, passphrase, function(){
    res.end();
  });
}

```

Copy the content in to-be-copied into nodejs-server-server.

Add the following dependencies to the package.json in nodejs-server-server:

```
    "superagent" : "2.3.0",
    "async" : "2.1.2",
    "jsondiffpatch" : "0.2.4",
    "filereader" : "0.10.3",
    "extend": "3.0.0",
    "blake2s": "1.1.0",
    "jquery" : "3.1.1",
    "pkgcloud" : "1.4.0",
    "nconf" : "0.8.4",
    "request" : "2.79.0",
    "chunking-streams" : "0.0.8"
```

Set the E2EE server address in config.json.

To start the server, go into nodejs-server-server and execute:

```
npm start
```

This starts a server which should be accessed from the same machine (otherwise it is not end-to-end encryption) using the client code generated from e2eeSwagger.json (see below).

## Integration with Openstack Swift

To enable communication with Swift, start WITDOM Swift Docker container:

```
docker run swift
```

## Run integration tests

Generate Javascript client code on [1] using e2eeSwagger.json in integration-tests. Move the generated javascript-client folder into e2ee-client.
Set basePath in javascript-client/src/ApiClient.js to point to the Node.js (proxy) server (see above).

Install code for communication with Node.js server:

```
npm install ./javascript-client
```

Note: the package.json in root folder is only due to integration test.

Go into integration-tests folder and execute:

```
nodejs client.js
```

# TODO

## WITDOM related

- include tokens
- TLS

## Other
- startCollectors
- configuration for e2ee-server address (see crypton.url())


[1] http://editor.swagger.io/#/


