FROM node:argon

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Bundle app source
COPY . /usr/src/app

# Install dependencies (necesarry when running integration tests from within e2ee client container)
RUN npm install
RUN npm install ./javascript-client

WORKDIR /usr/src/app/nodejs-server-server
RUN npm install

EXPOSE 8080
CMD [ "npm", "start" ]
