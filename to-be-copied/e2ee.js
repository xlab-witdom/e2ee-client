var fs = require('fs');
var nconf = require("nconf");
var pkgcloud = require('pkgcloud');
var chunkingStreams = require('chunking-streams');
var SizeChunker = chunkingStreams.SizeChunker;
var request = require('request');

var e2ee

(function() {
    'use strict';

    e2ee = {}
    e2ee.settings = {}

    exports.session = {
        isReady: false,
        cryptonSession: null,
        indexContainer: null,
        keys: {},
        keyPairReady: false,
        currentFile: {
            fileObject: null,
            fileName: '',
            encryptedChunks: [],
            decryptedChunks: []
        }
    }

    e2ee.util = {}

    e2ee.util.resetCurrentFile = function() {
        delete exports.session.currentFile
        exports.session.currentFile = {
            fileObject: null,
            fileName: '',
            encryptedChunks: [],
            decryptedChunks: [],
            streamEncryptor: null,
            streamDecryptor: null
        }
    }

    exports.crypto = {}

    // Chunk size (in bytes)
    // Warning: Must not be less than 256 bytes
    exports.crypto.chunkSize = 1024 * 1024 * 1

    exports.crypto.addChunkToContainer = function(chunk_id, chunk, fileContainer) {
        fileContainer.get('chunks', function(err, chunks) {
            if (err) {
                console.error(err)
                console.error('value for chunks key of the file (that is being encrypted) container cannot be retrieved')
            } else {
                chunks[chunk_id] = chunk
            }
        })
    }

    exports.crypto.deleteFile = function(fileName, callback) {
        exports.session.cryptonSession.deleteContainer(fileName, function(err, container) {
            if (err) {
                console.error(err)
            } else {
                callback()
            }
        })
    }

    exports.crypto.notifyAboutUnshared = function(file, callback) {
        // when file that was shared is deleted, the unshare notification should be sent
        exports.session.cryptonSession.getPeer(file.user, function callback1(err, peer) {
            if (err) {
                if (window.console && window.console.log) {
                    console.error(err)
                }
            }
            var headers = {}
            var payload = {
                operation: 'unshare',
                fileName: file.fileName
            }
            peer.sendMessage(headers, payload, function(err, messageId) {
                if (err) {
                    console.err(err)
                }
                callback()
            })
        })
    }

    exports.crypto.getContainerByHmac = function(fileHmacName, peerName, fileName, callback1) {
        exports.session.cryptonSession.getPeer(peerName, function callback(err, peer) {
            if (err) {
                console.error(err)
                return
            }
            exports.session.cryptonSession.loadWithHmac(fileHmacName, peer, function(err, fileContainer) {
                if (err) {
                    console.info(err)
                } else {
                    fileContainer.name = fileName // when loading with hmac, file name is not set
                    callback1(fileContainer)
                }
            })
        })
    }

    exports.crypto.downloadFileByHmac = function(hmac, peerName, fileName) {
        exports.crypto.getContainerByHmac(hmac, peerName, fileName, function(fileContainer) {
            fileContainer.get('chunks', function(err, chunks) {
                if (err) {
                    console.info(err)
                    console.info('file not available: ' + hmac)
                } else {
                    // this is to be replace with SWIFT object:
                    var filePath = 'README.md';
                    var f = fs.createWriteStream(filePath);
                    Object.keys(chunks).forEach(function(key) { // keys are data positions
                        var a = chunks[key]
                        // convert object to array of values:
                        var arr = Object.keys(a).map(function(key) {
                            return a[key]
                        })
                        //var v = String.fromCharCode.apply(null, arr) // if you want to see the plaintext
                        var b = new Buffer(arr)
                        f.write(b);
                    });
                    f.end();
                }
            })
        })
    }

    exports.crypto.getContainer = function(name, callbackUrl, callback) {
        exports.session.cryptonSession.load(name, function(err, fileContainer) {
            if (err) {
                var success = false;
                var msg = "Failed to retrieve container '" + name + "' (" + err + ")";
                console.log("<ERROR> " + msg);
                e2ee.callPO(callbackUrl, success, msg)
            } else {
                console.log("<INFO> Container '" + name + "' successfully loaded");
                callback(fileContainer)
            }
        })
    }

    exports.crypto.downloadFile = function(fileName, swiftContainerName, swiftObjectName, callbackUrl) {
        exports.session.indexContainer.get('fileNames', function(err, fileNames) {
            if (err) {
                console.info(err)
                console.info('file names from indexContainer could not be retrieved')
                return
            }
            var filesList = fileNames['notMineFiles'] //# <--?
            for (var i = 0; i < filesList.length; i++) {
                var file = filesList[i]
                if (file.fileName === fileName) {
                    exports.crypto.downloadFileByHmac(file.hmac, file.peer, file.fileName)
                    console.log(fileName);
                    return
                }
            }


            exports.crypto.getContainer(fileName, callbackUrl, function(fileContainer) {
                fileContainer.get('chunks', function(err, chunks) {
                    if (err) {
                        var msg = 'file not available: ' + fileName
                        console.info(msg)
                        var success = false
                        console.log();
                        e2ee.callPO(callbackUrl, success, msg)
                    } else {
                        nconf.file({
                            file: '../config.json'
                        });
                        var saveTo = nconf.get('saveDecryptedFileTo');

                        var uploadToSwift = false;
                        if (saveTo == "swift") {
                            uploadToSwift = true;
                        }

                        if (uploadToSwift) {
                            console.log("uploading to Swift");
                            console.log(swiftContainerName)
                            console.log(swiftObjectName)

                            var swiftAddress = nconf.get('swift-address');
                            var swiftPort = nconf.get('swift-port');
                            var swiftUsername = nconf.get('swift-username'); // todo
                            var swiftPassword = nconf.get('swift-password'); // todo

                            var client = pkgcloud.storage.createClient({
                                provider: 'openstack',
                                username: swiftUsername,
                                password: swiftPassword,
                                authUrl: swiftAddress + ":" + swiftPort,
                                version: 1
                            });

                            var writeStream = client.upload({
                                container: swiftContainerName,
                                remote: swiftObjectName
                            });

                            Object.keys(chunks).forEach(function(key) { // keys are data positions
                                var a = chunks[key]
                                // convert object to array of values:
                                var arr = Object.keys(a).map(function(key) {
                                    return a[key]
                                })
                                //var v = String.fromCharCode.apply(null, arr) // if you want to see the plaintext
                                var b = new Buffer(arr)
                                writeStream.write(b);
                            });
			    writeStream.end("writeStream end");
			    writeStream.on("finish", function() {
                                var msg = "File was decrypted and uploaded to Swift."
                                var success = true
                                e2ee.callPO(callbackUrl, success, msg)
			    });			    
                        } else { // to local file
                            var filePath = fileName;
                            console.log("writing to file:");
                            console.log(filePath);
                            var f = fs.createWriteStream(filePath);
                            Object.keys(chunks).forEach(function(key) { // keys are data positions
                                var a = chunks[key]
                                // convert object to array of values:
                                var arr = Object.keys(a).map(function(key) {
                                    return a[key]
                                })
                                //var v = String.fromCharCode.apply(null, arr) // if you want to see the plaintext
                                var b = new Buffer(arr)
                                f.write(b);
                            });
                            f.end();
                        }
                    }
                })
            })
        })
    }

    exports.crypto.encryptLocalFile = function(file) {
        exports.session.currentFile.fileName = file.name

        exports.session.cryptonSession.load(file.name, function(err, fileContainer) {
            console.log("<INFO> Trying to load Crypton container...")
            if (err) { // the file is not stored on Crypton yet
                console.log(err)
                exports.session.cryptonSession.create(file.name, function(err, fileContainer) {
                    console.log("<INFO> Creating Crypton container for file [" + file.name + "]");
                    if (err) {
                        console.error(err)
                        console.error('<ERROR> Crypton container for the file that is being encrypted could not be created')
                    } else {
                        console.log("<INFO> Creation of Crypton container successful. Chunks key will be added to fileContainer")
                        fileContainer.add('chunks', function() {
                            var alreadyExists = false
                            exports.crypto.addNextChunkToContainer(file, fileContainer, 0, alreadyExists)
                        })
                    }
                })
            } else { // the file will be overwritten on Crypton
                console.log("<INFO> Encrypted file already exists and will be overwritten in Crypton");
                var alreadyExists = true
                exports.crypto.addNextChunkToContainer(file, fileContainer, 0, alreadyExists)
            }
        })
    }

    exports.crypto.addNextChunkToContainer = function(file, fileContainer, dataPosition, alreadyExists) {
        console.log("adding next chunk ...")
        e2ee.file.read(
            file,
            dataPosition,
            dataPosition + exports.crypto.chunkSize,
            function(chunk) {
                var isLast = false
                if (dataPosition >= (file.size - exports.crypto.chunkSize)) {
                    isLast = true
                }
                var encryptedChunk

                exports.crypto.addChunkToContainer(dataPosition, chunk, fileContainer) // todo: this contains async call, but is used as sync
                if (isLast) {
                    fileContainer.add('metadata', function() {
                        fileContainer.get('metadata', function(err, meta) {
                            if (err) {
                                console.error(err)
                                console.error('value for metadata key of the file (that is being encrypted) container cannot be retrieved')
                            } else {
                                var metadata = "added " + new Date().toJSON().slice(0, 10)
                                meta["date"] = metadata
                                fileContainer.save(function(err) {
                                    if (err) {
                                        console.info(err)
                                    } else {
                                        if (!alreadyExists) {
                                            exports.session.indexContainer.get('fileNames', function(err, fileNames) {
                                                fileNames['listOfFiles'].push(file.name)
                                                exports.session.indexContainer.save(function(err) {
                                                    if (err) {
                                                        console.error(err)
                                                        console.error('name of the file that is being encrypted could not be stored in indexContainer')
                                                    }
                                                })
                                            })
                                        }
                                        console.log('Encryption was successful.')
                                    }
                                })
                            }
                        })
                    })
                } else {
                    dataPosition += exports.crypto.chunkSize
                    return exports.crypto.addNextChunkToContainer(
                        file,
                        fileContainer,
                        dataPosition,
                        alreadyExists
                    )
                }
            }
        )
    }

    exports.crypto.encryptSwiftFile = function(swiftContainerName, fileName, callbackUrl) {
        console.log("<DEBUG> In encryptSwiftFile");
    console.log(" <DEBUG> exports.session.curentFile.fileName (before assignment) = " + exports.session.currentFile.fileName);
    exports.session.currentFile.fileName = fileName
    console.log(" <DEBUG> exports.session.curentFile.fileName (after assignment) = " + exports.session.currentFile.fileName);
    
        exports.session.cryptonSession.load(fileName, function(err, fileContainer) {
            console.log("<INFO> Trying to load Crypton container for file ["+ fileName +"]...");
            if (err) { // the file is not stored on Crypton yet
                console.log("<INFO> The file is not stored on Crypton, will create new container (" + err + ")");
                exports.session.cryptonSession.create(fileName, function(err, fileContainer) {
                    console.log("<INFO> Creating  Crypton container for file [" + fileName + "]");
                    if (err) {
                        console.error(err)
                        console.error('<ERROR> Failed to create Crypton container for the file that is being encrypted');
                        var success = false
                        e2ee.callPO(callbackUrl, success, err)
                    } else {
                        console.log("<INFO> Successfuly created new Crypton container. Chunks key will be added to fileContainer")
                        fileContainer.add('chunks', function() {
                            var alreadyExists = false
                            exports.crypto.addToContainer(swiftContainerName, fileName, fileContainer, alreadyExists, callbackUrl)
                        })
                    }
                })
            } else { // the file will be overwritten on Crypton
                console.log("<INFO> Container for the file already exists (no need to create a new container)")
                var alreadyExists = true
                exports.crypto.addToContainer(swiftContainerName, fileName, fileContainer, alreadyExists, callbackUrl)
            }
        })
    console.log("<DEBUG> End of encryptSwiftFile");
    }

    exports.crypto.addToContainer = function(swiftContainerName, fileName, fileContainer, alreadyExists, callbackUrl) {
    console.log("<DEBUG> exports.crypto.addToContainer");
        console.log(" <DEBUG> swiftContainerName = " + swiftContainerName);
        console.log(" <DEBUG> fileName = " + fileName);
        console.log(" <DEBUG> alreadyExists = " + alreadyExists);
        nconf.file({
            file: '../config.json'
        });
        var swiftAddress = nconf.get('swift-address');
        var swiftPort = nconf.get('swift-port');
        var swiftUsername = nconf.get('swift-username'); // todo
        var swiftPassword = nconf.get('swift-password'); // todo

        var client = pkgcloud.storage.createClient({
            provider: 'openstack',
            username: swiftUsername,
            password: swiftPassword,
            authUrl: swiftAddress + ":" + swiftPort,
            version: 1
        });


        var chunker = new SizeChunker({
            chunkSize: exports.crypto.chunkSize
        })

        var stream = client.download({
            container: swiftContainerName,
            remote: fileName
        });

        chunker.on('chunkStart', function(id, done) {
            done();
        });

        chunker.on('chunkEnd', function(id, done) {
            done();
        });

        var dataPosition = 0;

        chunker.on('data', function(chunk) {
            exports.crypto.addChunkToContainer(dataPosition, chunk.data, fileContainer) // todo: this contains async call, but is used as sync
            dataPosition += chunk.data.length
        });

        stream.pipe(chunker)
        stream.on('finish', function() {
            fileContainer.add('metadata', function() {
                fileContainer.get('metadata', function(err, meta) {
                    if (err) {
                        console.error(err)
                        console.error('value for metadata key of the file (that is being encrypted) container cannot be retrieved')
                        var success = false
                        e2ee.callPO(callbackUrl, success, err)
                    } else {
                        var metadata = "added " + new Date().toJSON().slice(0, 10)
                        meta["date"] = metadata
                        fileContainer.save(function(err) {
                            if (err) { // File has not changed
                                // In case file has not changes, report success, not 
                                console.info(err)
                                var success = true;//false
                                e2ee.callPO(callbackUrl, success, err)
                            } else {
                                if (!alreadyExists) {
                                    exports.session.indexContainer.get('fileNames', function(err, fileNames) {
                                        fileNames['listOfFiles'].push(fileName)
                                        exports.session.indexContainer.save(function(err) {
                                            if (err) {
                                                console.error(err)
                                                console.error('name of the file that is being encrypted could not be stored in indexContainer')
                                            }
                                        })
                                    })
                                }
                                var msg = 'Encryption was successful.'
                                console.log(msg)
                                var success = true
                                e2ee.callPO(callbackUrl, success, msg)
                            }
                        })
                    }
                })
            })

        });
    }

    e2ee.file = {}

    e2ee.file.read = function(file, start, end, callback, errorCallback) {
        if (end > file.size) {
            end = file.size
        }
        var buffer = new Buffer(end - start)
        fs.read(file.fd, buffer, 0, end - start, start)
        return callback(buffer)

    }

    e2ee.callPO = function(callbackUrl, success, msg) {
        console.log("Callback URL: " + callbackUrl)
        var status = success ? "success" : "failure";

        var options = {
            url: callbackUrl,
            headers: {
                'X-Auth-Token': 'todo'
            },
            json: {
                "status": status
            }
        };
        options['json']['results'] = {} 
        options['json']['results'][status] = msg


        function callback(error, response, body) {
            if (!error) {
                console.log("Received: " + response.statusCode);
                console.log(body);
            } else {
                console.log("An error ocurred during callPO -> callback " + error)
            }
        }

        console.log("Before request.post: (options) " + JSON.stringify(options))

        request.post(options, callback);
    }

    exports.encrypt = function encrypt(containerName, objectName, callbackUrl) {
        console.log("<INFO> File will be loaded for encryption");
        console.log(" <INFO> Swift container name: [" + containerName + "]");
        console.log(" <INFO> Swift object name: [" + objectName + "]");

        nconf.file({
            file: '../config.json'
        });
        var readFileFrom = nconf.get('readFileFrom');

        var takeFileFromSwift = false;
        if (readFileFrom == "swift") {
            takeFileFromSwift = true;
        }

        if (takeFileFromSwift) {
            e2ee.util.resetCurrentFile()
            //exports.session.currentFile.fileObject = file // todo
            exports.crypto.encryptSwiftFile(containerName, objectName, callbackUrl)
        } else {
            var filePath = objectName;
            var fs = require("fs");
            var stats = fs.statSync(filePath)
            var fileSizeInBytes = stats["size"]

            fs.open(filePath, 'r', (err, fd) => {
                if (err) {
                    console.log(err);
                }
                var file = {
                    fd: fd,
                    name: objectName,
                    size: fileSizeInBytes
                }

                e2ee.util.resetCurrentFile()
                exports.session.currentFile.fileObject = file
                exports.crypto.encryptLocalFile(file)
            });
        }
    }
})()
