var nconf = require("nconf");
var pkgcloud = require('pkgcloud');
fs = require('fs');

var E2EeClientApi = require('e2_ee_client_api');

var api = new E2EeClientApi.DefaultApi()

var xAuthToken = "xAuthToken_example";
var payload = new E2EeClientApi.E2eeLoginCredentials();
payload.passphrase = "blatra";

nconf.file({file: '../config.json'});
var swiftAddress = nconf.get('swift-address');
var swiftPort = nconf.get('swift-port');
var swiftUsername = nconf.get('swift-username'); // todo
var swiftPassword = nconf.get('swift-password'); // todo

var containerName = "testContainer"
var fileName = 'e2eeSwagger.json'

var client = pkgcloud.storage.createClient({
        provider: 'openstack',
        username: swiftUsername,
        password: swiftPassword,
        authUrl:  swiftAddress + ":" + swiftPort,
        version: 1
    });

// create container on Swift:
client.createContainer({name: containerName}, function (err, container) {
    if (err) {
        console.log(err);
        return;
    }
});

// upload some file to Swift (which we will later encrypt):
var readStream = fs.createReadStream(fileName);
var writeStream = client.upload({
    container: containerName,
    remote: fileName // not neccessarily the same as original name
});


writeStream.on('error', function(err) {
// handle your error case
});

writeStream.on('success', function(file) {
// success, file will be a File model
});

readStream.pipe(writeStream);

// which object to retrieve from Swift, to encrypt it and put it into E2EE server
var sourceLoc = new E2EeClientApi.DataLocation();
sourceLoc.host = swiftAddress
sourceLoc.port = swiftPort
sourceLoc.containerName = containerName
sourceLoc.objectName = fileName

// where to put decrypted object
var destLoc = new E2EeClientApi.DataLocation();
destLoc.host = swiftAddress
destLoc.port = swiftPort
destLoc.containerName = containerName
destLoc.objectName = "e2eeSwagger-from-e2ee-server.json"

var callbackUrl = "127.0.0.1:8080";//"some url"

var decryptCallback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('decrypt call was successfull - means that decryption is going on now.');
  }
};

var encryptCallback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('encrypt call was successfull - means that encryption is going on now.');
    // wait some time for encryption to be finished:

    setTimeout(function(){
      var decryptPayload = new E2EeClientApi.DecryptParams();
      decryptPayload.location = destLoc
      decryptPayload.callbackURL = callbackUrl
      decryptPayload.objectName = fileName // name of the object that was encrypted

      api.decryptPost(xAuthToken, decryptPayload, decryptCallback);
    }, 15000);
  }
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('openSession call was successfull.');

    var encryptPayload = new E2EeClientApi.EncryptParams();
    encryptPayload.location = sourceLoc
    encryptPayload.callbackURL = callbackUrl

    api.encryptPost(xAuthToken, encryptPayload, encryptCallback);

  }
};

api.openSessionPost(xAuthToken, payload, callback);


