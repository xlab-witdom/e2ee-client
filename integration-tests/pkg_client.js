// just for testing pkgcloud

var chunkingStreams = require('chunking-streams');
var SizeChunker = chunkingStreams.SizeChunker;

var pkgcloud = require('pkgcloud');
fs = require('fs');

var containerName= "testContainer"
//var fileName = "e2eeSwagger.json"
var fileName = "restoredTestObject"

var client = pkgcloud.storage.createClient({
        provider: 'openstack',
        username: 'test:tester',
        password: 'testing',
        authUrl:  'http://172.17.0.4:8080',
        version: 1
    });

/*
client.createContainer({name: containerName}, function(err, container) {
    if (err) {
	console.log('-----------');
        console.log(err);
        return;
    }
});

client.getContainers(function (err, containers) {
    if (err) {
	console.log('-----------');
        console.log(err);
        return;
    }
    console.log('++++');
    console.log(containers);
});
*/

/*
  var readStream = fs.createReadStream(fileName);
  //var readStream = fs.createReadStream('bla.txt');
  var writeStream = client.upload({
    container: containerName,
    remote: fileName
  });

  writeStream.on('error', function(err) {
    // handle your error case
  });

  writeStream.on('success', function(file) {
    // success, file will be a File model
  });

  readStream.pipe(writeStream);
*/

var bla = "e2eeSwagger-from-e2ee-server.json"

var stream = client.download({
    //container: 'containerbla',
    //remote: 'remote-file-name.txt'
    container: containerName,
    remote: fileName
    //remote: bla
  });


var output

chunker = new SizeChunker({
        chunkSize: 10000000
    })

chunker.on('chunkStart', function(id, done) {
    output = fs.createWriteStream('./output-' + id);
    done();
});
 
chunker.on('chunkEnd', function(id, done) {
    output.end();
    done();
});
 
chunker.on('data', function(chunk) {
    output.write(chunk.data);
    console.log(chunk.id)
    console.log(chunk.data)
    console.log(chunk.data.length);
    
});
stream.pipe(chunker)



