CONTAINER=e2ee-client
IMAGE=e2ee-client

# Ports where e2ee-client is accessible (on local machine and in docker container)
MAPPED_PORT=8080
DOCKER_PORT=8080

# Public address of the machine running docker containers
# Should be changed via command-line or here, localhost will not work for communication between Docker containers
####### CHANGE THIS #######
PUBLIC_IP="localhost"

# IP:port endpoint to the e2ee-server
E2EE_SERVER_ADDRESS="https://$(PUBLIC_IP)"
E2EE_SERVER_PORT=4443

# Port where swift object storage is accesible
SWIFT_ADDRESS="http://$(PUBLIC_IP)"
SWIFT_PORT=8081

all: generate_config build run

build:
	docker build -t $(IMAGE) .
	
run:
	docker run -id \
		--publish $(MAPPED_PORT):$(DOCKER_PORT) \
		--name $(CONTAINER) \
		$(IMAGE)

logs:
	docker logs $(CONTAINER)

bash: 
	docker exec -it $(CONTAINER) /bin/bash

generate_config:
	./write_config.sh $(E2EE_SERVER_ADDRESS) $(E2EE_SERVER_PORT) $(SWIFT_ADDRESS) $(SWIFT_PORT)

clean:
	-docker stop $(CONTAINER)
	-docker rm $(CONTAINER)