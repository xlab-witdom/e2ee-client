#!/bin/bash

cat << EOF > ./config.json
	{
	  "readFileFrom": "swift", 
	  "saveDecryptedFileTo": "swift", 
	  "e2ee-server-address": "$1:$2",
	  "swift-address": "$3",
	  "swift-port": "$4",
	  "swift-username": "test:tester",
	  "swift-password": "testing"
	}
EOF